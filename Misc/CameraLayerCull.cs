﻿using UnityEngine;
using System.Collections;

public class CameraLayerCull: MonoBehaviour {
	
	public float treeCullDist = 180f;
    public float CaveCullDist = 5f;
	
	
	// Use this for initialization
	void Start() {
		float[] distances = new float[32];
		distances[10] = treeCullDist;
	    distances[9] =  CaveCullDist;
		//distances[10] = grassCullDist;
		GetComponent < Camera > ().layerCullDistances = distances;
	}
	
	
}
