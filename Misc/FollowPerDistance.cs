﻿using UnityEngine;
using System.Collections;

public class FollowPerDistance: MonoBehaviour {
	[SerializeField]
	private float distance;
	[SerializeField]
	private Transform target;
	[SerializeField]
	private float height;

	// Use this for initialization
	void Start() {
		transform.position = new Vector3(transform.position.x, target.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update() {
		
	}
	void LateUpdate() {
		if (Vector3.Distance(transform.position, target.position) > distance) {
			//float step = speed * Time.deltaTime;
			//transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.position.x,target.position.y + height,target.position.z), step);
			transform.position = target.position;
		}
		
		
	}
}
