﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class HullPooler : MonoBehaviour {
    public GameObject hullObject;
    public int pooledAmount = 6084;
    [SerializeField]
    List<GameObject> pooledObjects;

    int lowIndex = 0;
    int highIndex;
    //false for highIndex = true for lowIndex = false
    bool lowOrHigh = false;
    // Use this for initialization
    void Start () {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(hullObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);

        }
        highIndex = pooledObjects.Count-1;
    }

    public GameObject getObject()
    {
        if (!lowOrHigh)
        {
            if (!pooledObjects[lowIndex].activeInHierarchy)
            {
                lowOrHigh = true;
                GameObject go = pooledObjects[lowIndex];
                lowIndex++;
                return go;
            }
         
           
        }

        else
        {
            if (!pooledObjects[highIndex].activeInHierarchy)
            {
                lowOrHigh = false;
                GameObject go = pooledObjects[highIndex];
                highIndex--;
                return go;
            }
        }

        return null;
    }

    public void reset()
    {
        lowIndex = 0;
        highIndex = pooledObjects.Count-1;
        //false for highIndex = true for lowIndex = false
         lowOrHigh = false;
    }

}
