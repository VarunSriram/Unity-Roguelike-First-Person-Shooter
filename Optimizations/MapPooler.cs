﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPooler : MonoBehaviour {
    public GameObject pooledMapGenerator;
    public int pooledAmount = 49;
    public bool willGrow = true;

    [SerializeField]
    List<GameObject> pooledMapGenerators;
    
	// Use this for initialization
	void Start () {
        pooledMapGenerators = new List<GameObject>();
        for(int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject) Instantiate(pooledMapGenerator);
            obj.SetActive(false);
            pooledMapGenerators.Add(obj);

        }
	}
	
    public GameObject getMapGenerator()
    {
        for(int i = 0; i < pooledMapGenerators.Count; i++)
        {
            if (!pooledMapGenerators[i].activeInHierarchy)
            {
                return pooledMapGenerators[i];
            }
        }

        if (willGrow)
        {
            GameObject obj = (GameObject)Instantiate(pooledMapGenerator);
            pooledMapGenerators.Add(obj);
            return obj;
        }

        return null;
    }
	
}
