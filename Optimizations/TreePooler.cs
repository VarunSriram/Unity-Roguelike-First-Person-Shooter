﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreePooler : MonoBehaviour
{

    public List<GameObject> pooledObjects;
    public int pooledAmount = 10;
    public bool willGrow = true;
    
   public Dictionary<GameObject, List<GameObject>> treeDictionary;
   
    
    

    // Use this for initialization
    void Start()
    {
        treeDictionary = new Dictionary<GameObject, List<GameObject>>();

        for(int i = 0; i < pooledObjects.Count; i++) {
       
        List<GameObject> treeList = new List<GameObject>();
        for (int q = 0; q < pooledAmount; q++)
        {
            GameObject obj = (GameObject)Instantiate(pooledObjects[i]);
            obj.SetActive(false);
            treeList.Add(obj);
            

        }

        treeDictionary.Add(pooledObjects[i], treeList);

        }
    }

    public GameObject getObject(int indexValue)
    {
       
        if (treeDictionary.ContainsKey(pooledObjects[indexValue])) {

            List<GameObject> treeList = treeDictionary[pooledObjects[indexValue]];
           
            for (int i = 0; i < treeList.Count; i++)
            {
                if (!treeList[i].activeInHierarchy)
                {
                    return treeList[i];
                }
            }

            if (willGrow)
            {
                GameObject obj = (GameObject)Instantiate(pooledObjects[indexValue]);
                treeList.Add(obj);
                return obj;
            }

            return null;
        }
        return null;
    }

    public int getNumberOfPooledObjects()
    {
        return pooledObjects.Count;
    }
}
