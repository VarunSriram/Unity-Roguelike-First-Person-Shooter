﻿using UnityEngine;
using System.Collections;
//Class Responisble for converting the 2D coordinate system in the dungeon blueprints into 3D coordinates of the scene space. 
public class SceneSpaceCalulator{

	private static SceneSpaceCalulator singletonInstance;
	private SceneSpaceCalulator(){

	}

	public static SceneSpaceCalulator getInstance(){
		if (singletonInstance == null) {
			singletonInstance = new SceneSpaceCalulator();

		}
		return singletonInstance;

	}


	//Returns coordinate of particular room given a position in the scenespace and the room size of the dungeon. Room coordinate is returned in the form of a vector2.
	public Vector2 vector3ToDungeonCoordinate(Vector3 worldOrigin, Vector3 position,int roomSize){
		Vector3 relLocation = position - worldOrigin;
		float xCoord = Mathf.Floor(relLocation.x / roomSize);
		float yCoord = Mathf.Floor(relLocation.z / roomSize);
		return new Vector2 (xCoord, yCoord);
	}

    //Returns 3D coordinate given an x and y position in the dungeon blueprint.
	public Vector3 dungeonCoordinateToVector3(Vector3 worldOrigin,int xCoord,int yCoord,int roomSize){
		float x = (float)xCoord * (float)roomSize;
		float z = (float)yCoord * (float)roomSize;
		Vector3 location = new Vector3 (x, 0, z) + worldOrigin;
		return location;
	}
    //Returns coordinate of a particular tile given a position in the scene space.
	public Vector2 vector3ToRoomCoordinate(Vector3 worldOrigin,Vector3 position, int roomSize,int tileSize){
		Vector2 dungeonCoord = this.vector3ToDungeonCoordinate (worldOrigin, position, roomSize);
		Vector3 roomOrigin = this.dungeonCoordinateToVector3 (worldOrigin, (int) dungeonCoord.x,(int) dungeonCoord.y,roomSize);
		Vector3 relLocation = position - roomOrigin;
		float xCoord = Mathf.Floor (relLocation.x / tileSize);
		float yCoord = Mathf.Floor (relLocation.z/tileSize);
		return new Vector2(xCoord,yCoord);
	}
    //Returns 3D coordinate given a x and y coordinate inside a room.
	public Vector3 roomCoordinateToVector3(Vector3 worldOrigin,int xCoord,int yCoord,int dungeonX,int dungeonY, int roomSize,int tileSize){
		Vector3 roomOrigin = this.dungeonCoordinateToVector3 (worldOrigin, dungeonX, dungeonY, roomSize);
		float x = (float)xCoord * (float)tileSize;
		float z = (float)yCoord * (float)tileSize;
		return new Vector3(x,0f,z) + roomOrigin + worldOrigin;
	}







}
