﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GlobalPathRequestManager : MonoBehaviour {

    public List<PathRequestManager> pathMgmtList = new List<PathRequestManager>();
    private static GlobalPathRequestManager instance;

	void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        instance = this;

        DontDestroyOnLoad(gameObject);
    }

    public static GlobalPathRequestManager getInstance()
    {
        return instance;
    }

    public void requestPath(int worldNum, Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        pathMgmtList[worldNum].RequestPath(pathStart, pathEnd, callback);
    }
}
