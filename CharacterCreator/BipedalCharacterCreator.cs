﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BipedalCharacterCreator: MonoBehaviour {
	private Transform legPort;
	private Transform armPort;
	private Transform headPort;
	// Use this for initialization
	//On start find the approriate limb ports and add the arms and legs and heads to the torso.
	void Start() {
		legPort = transform.Find("leg_port");
		armPort = transform.Find("arm_port");
		headPort = transform.Find("head_port");
		this.addArms();
		this.addLegs();
		this.addHead();
	}
	public void addArms() {
		GameObject[] gos;
		gos = Resources.LoadAll < GameObject > ("Multiverse/Assets/BodyParts/BipedalArms");
		Debug.Log(gos.Length);
		GameObject toSpawn = gos[Random.Range(0, gos.Length)];
		GameObject spawned = Instantiate(toSpawn, armPort.position, armPort.rotation) as GameObject;
		spawned.transform.parent = armPort;
		
	}
	
	public void addLegs() {
		GameObject[] gos;
		gos = Resources.LoadAll < GameObject > ("Multiverse/Assets/BodyParts/BipedalLegs");
		GameObject toSpawn = gos[Random.Range(0, gos.Length)];
		GameObject spawned = Instantiate(toSpawn, legPort.position, legPort.rotation) as GameObject;
		spawned.transform.parent = legPort;
	}
	
	public void addHead() {
		GameObject[] gos;
		gos = Resources.LoadAll < GameObject > ("Multiverse/Assets/BodyParts/Heads");
		GameObject toSpawn = gos[Random.Range(0, gos.Length)];
		GameObject spawned = Instantiate(toSpawn, headPort.position, headPort.rotation) as GameObject;
		spawned.transform.parent = headPort;
	}
	
}
