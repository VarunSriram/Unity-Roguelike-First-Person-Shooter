﻿using UnityEngine;
using System.Collections;

public class UVTextureEditor: MonoBehaviour {
	private Texture2D uvMap;
	private Color basecolor;
	private float offsetColor;

	private void Start() {
		uvMap = (Texture2D) GetComponent < Renderer > ().material.mainTexture;
		this.changeColor();
	}
	
	public void changeColor() {
		
		basecolor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
		
		Color[] Pixels = uvMap.GetPixels(); // Get the whole Color Array (More Efficient)
		
		for (int x = 0; x < this.uvMap.width; x++) {
			for (int y = 0; y < this.uvMap.height; y++) {
				
				Color CurrPix = Pixels[x + y * uvMap.width];
				
				if (CurrPix.r != 0f && CurrPix.g != 0f && CurrPix.b != 0f) {
//					float coloff = Random.Range(0f, offsetColor);
					
					
					Pixels[x + y * uvMap.width] = basecolor;
				}
			}
		}
		uvMap.SetPixels(Pixels);
		uvMap.Apply();
		Renderer r = GetComponent < Renderer > ();
		r.material.mainTexture = uvMap;
	}
	
}