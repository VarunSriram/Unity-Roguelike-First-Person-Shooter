﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapGenerator : MonoBehaviour
{

    public int width;
    public int height;
    public float squareSize = 1f;
    public int temp;
    public int rain;
    
     
    public string seed;
    public bool useRandomSeed;
    public bool debugToggle = false;
    public bool debugGizmos = false;
    public GroundFloor gf;
    

    public Grid collisionMap;
    public HullPooler collisionPooler;


    public Material floor;
    public Material grass;
    public Material wall;

    [Range(0, 100)]
    public int randomFillPercent;

    public int[,] map;
    Room roomType;
    List<Coord> wallTiles;
    List<Vector2> floorTiles;

    List<Vector2> vegRegion;

    void Start()
    {
      //  GenerateMap();
    }

    void Update()
    {
        collisionMap.displayGridGizmos = debugGizmos;
        if (debugToggle)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GenerateMap();
               
                gf.buildMesh(width, height, squareSize,floor,grass,temp,rain);
                

            }
        }
        
    }

    

    public void GenerateMap()
    {
        wallTiles = new List<Coord>();
        floorTiles = new List<Vector2>();
        vegRegion = new List<Vector2>();

        float mapCreateTime = Time.realtimeSinceStartup;

        map = new int[width, height];
        RandomFillMap();

        for (int i = 0; i < 5; i++)
        {
            SmoothMap();
        }

        ProcessMap();

        int borderSize = 2;
        int[,] borderedMap = new int[width + borderSize * 2, height + borderSize * 2];

        for (int x = 0; x < borderedMap.GetLength(0); x++)
        {
            for (int y = 0; y < borderedMap.GetLength(1); y++)
            {
                if (x >= borderSize && x < width + borderSize && y >= borderSize && y < height + borderSize)
                {
                    borderedMap[x, y] = map[x - borderSize, y - borderSize];
                }
                else
                {
                    borderedMap[x, y] = 1;
                }
            }
        }
   //     Debug.Log("MAP CREATE Time = " + (Time.realtimeSinceStartup - mapCreateTime));
    //    float sortStartTime = Time.realtimeSinceStartup;
        this.sortTiles();
  //      Debug.Log("Sort Time = " + (Time.realtimeSinceStartup - sortStartTime));

        MeshGenerator meshGen = GetComponent<MeshGenerator>();

   //     float meshGenTime = Time.realtimeSinceStartup;
        meshGen.GenerateMesh(borderedMap, squareSize);
        meshGen.setWallMaterial(wall);
 //       Debug.Log("MESH CREATE Time = " + (Time.realtimeSinceStartup - meshGenTime));


        gf.transform.localPosition = new Vector3((-(float)width * squareSize) / 2f, -3f*squareSize, (-(float)height * squareSize) / 2f);
        gf.buildMesh(width, height, squareSize, floor, grass,temp,rain);

        if (debugToggle)
        {
            this.createCollisionMap();
        }
        
    }

    void ProcessMap()
    {
        this.createLeftDoorSlot();
        this.createRightDoorSlot();
        //Carve the door slots here\
       if(roomType != null)
        {
            if(roomType is TypeOneRoom)
            {
                this.createLeftDoorSlot();
                this.createRightDoorSlot();
            }

            if(roomType is TypeTwoRoom)
            {
                this.createLeftDoorSlot();
                this.createRightDoorSlot();
                this.createBottomDoorSlot();
            }

            if(roomType is TypeThreeRoom)
            {
                this.createLeftDoorSlot();
                this.createRightDoorSlot();
                this.createTopDoorSlot();
            }

            if(roomType is TypeFourRoom)
            {
                this.createLeftDoorSlot();
                this.createRightDoorSlot();
                this.createTopDoorSlot();
                this.createBottomDoorSlot();
            }
        }


        List<List<Coord>> wallRegions = GetRegions(1);
        int wallThresholdSize = 50;

        foreach (List<Coord> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Coord tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = 0;
                }
            }
        }

        List<List<Coord>> roomRegions = GetRegions(0);
        int roomThresholdSize = 50;
        List<Carving> survivingRooms = new List<Carving>();

        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coord tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = 1;
                }
            }
            else
            {
                survivingRooms.Add(new Carving(roomRegion, map));
            }
        }
        survivingRooms.Sort();
        survivingRooms[0].isMainRoom = true;
        survivingRooms[0].isAccessableFromMainRoom = true;

        ConnectClosestRooms(survivingRooms);
    }

    void createRightDoorSlot()
    {
        int centXCoord = this.width / 2;
        int centYCoord = this.height - 2;
        for(int i = centXCoord-2; i<centXCoord+3; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                map[i,centYCoord-j] = 0;
            }
        }
    }

    void createLeftDoorSlot()
    {
        int centXCoord = this.width / 2;
        int centYCoord = 1;
        for (int i = centXCoord - 2; i < centXCoord + 3; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                map[i, centYCoord + j] = 0;
            }
        }
    }

    void createTopDoorSlot()
    {
        int centXCoord = 1;
        int centYCoord = this.height/2;
        for (int j = centYCoord - 2; j < centYCoord + 3; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                map[centXCoord+i,j] = 0;
            }
        }
    }

    void createBottomDoorSlot()
    {
        int centXCoord = this.width - 2;
        int centYCoord = this.height / 2;
        for (int j = centYCoord - 2; j < centYCoord + 3; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                map[centXCoord - i, j] = 0;
            }
        }
    }

    void ConnectClosestRooms(List<Carving> allRooms, bool forceAccesibilityFromMainRoom = false)
    {
        List<Carving> roomListA = new List<Carving>();
        List<Carving> roomListB = new List<Carving>();

        if (forceAccesibilityFromMainRoom)
        {
            foreach(Carving room in allRooms)
            {
                if (room.isAccessableFromMainRoom)
                {
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int bestDistance = 0;
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();
        Carving bestRoomA = new Carving();
        Carving bestRoomB = new Carving();
        bool possibleConnectionFound = false;

        foreach (Carving roomA in roomListA)
        {
            if (!forceAccesibilityFromMainRoom)
            {
                possibleConnectionFound = false;
                if(roomA.connectedRooms.Count > 0)
                {
                    continue;
                }
            }

            possibleConnectionFound = false;

            foreach (Carving roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB))
                {
                    continue;
                }
               

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
                    {
                        Coord tileA = roomA.edgeTiles[tileIndexA];
                        Coord tileB = roomB.edgeTiles[tileIndexB];
                        int distanceBetweenRooms = (int)(((tileA.tileX - tileB.tileX)* (tileA.tileX - tileB.tileX)) + ((tileA.tileY - tileB.tileY)* (tileA.tileY - tileB.tileY)));

                        if (distanceBetweenRooms < bestDistance || !possibleConnectionFound)
                        {
                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            if (possibleConnectionFound && !forceAccesibilityFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }
        if (possibleConnectionFound && forceAccesibilityFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(allRooms, true);
        }


        if (!forceAccesibilityFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    void CreatePassage(Carving roomA, Carving roomB, Coord tileA, Coord tileB)
    {
        Carving.ConnectRooms(roomA, roomB);

        if (debugToggle)
        {
            Debug.DrawLine(CoordToWorldPoint(tileA), CoordToWorldPoint(tileB), Color.green, 2);
        }

        List<Coord> line = GetLine(tileA, tileB);
        foreach(Coord c in line)
        {
            drawCircle(c, 15);
        }
    }

    void drawCircle(Coord c, int r)
    {
        for (int x = -r; x <= r; x++)
        {
            for (int y = -r; y <= r; y++)
            {
                if(x*x + y*y <= r * r)
                {
                    int drawX = c.tileX + x;
                    int drawY = c.tileY + y;
                    if (IsInMapRange(drawX, drawY))
                    {
                        map[drawX, drawY] = 0;
                    }
                }
            }
        }
    }

    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();
        int x = from.tileX;
        int y = from.tileY;

        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;

        bool inverted = false;
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if(longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for ( int i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));

            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if(gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }

        return line;


    }

    public Vector3 CoordToWorldPoint(int x, int y)
    {
        return CoordToWorldPoint(new Coord(x, y));
    }

    Vector3 CoordToWorldPoint(Coord tile)
    {
        return transform.position + new Vector3(((-(float)width*squareSize) / 2) + (squareSize/2f) + (tile.tileX*squareSize), 0f, ((-(float)height*squareSize) / 2) + (squareSize/2f) + (tile.tileY*squareSize));
    }


    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coord> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Coord tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;
                    }
                }
            }
        }

        return regions;
    }

    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();
        int[,] mapFlags = new int[width, height];
        int tileType = map[startX, startY];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coord(x, y));
                        }
                    }
                }
            }
        }

        return tiles;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }


    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    map[x, y] = 1;
                else if (neighbourWallTiles < 4)
                    map[x, y] = 0;

            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }


    public void createCollisionMap()
    {

        collisionMap.gridWorldSize = new Vector2((float)width * squareSize, (float)height * squareSize);
        collisionMap.nodeRadius = squareSize / 2f;

       float loopStart = Time.realtimeSinceStartup;

        List<GameObject> gos = new List<GameObject>();
        for (int i = 0; i < wallTiles.Count; i++)
        {
            float getObjectStart = Time.realtimeSinceStartup;
            GameObject go = collisionPooler.getObject();
         //   Debug.Log("GET HULL TIME: " + (Time.realtimeSinceStartup - getObjectStart));
            go.SetActive(true);
            float placeTime = Time.realtimeSinceStartup;
            go.transform.position = this.CoordToWorldPoint(wallTiles[i].tileX, wallTiles[i].tileY);
        //    Debug.Log("SET POSITION TIME: " + (Time.realtimeSinceStartup - placeTime));
            float setParentTime = Time.realtimeSinceStartup;
            go.transform.parent = transform;
        //    Debug.Log("SET PARENT TIME: " + (Time.realtimeSinceStartup - setParentTime));
            gos.Add(go);
        }
        
     //   Debug.Log("Hull Placement Time: " + (Time.realtimeSinceStartup - loopStart) +" "+wallTiles.Count);
       
        collisionMap.CreateGrid();
        
        float removeHullStart = Time.realtimeSinceStartup;
        for (int i = 0; i < gos.Count; i++)
        {
            gos[i].transform.parent = null;
            gos[i].SetActive(false);
        }
        collisionPooler.reset();

       

    }

    struct Coord
    {
        public int tileX;
        public int tileY;

        public Coord(int x, int y)
        {
            tileX = x;
            tileY = y;
        }
    }


    class Carving : IComparable<Carving>
    {
        public List<Coord> tiles;
        public List<Coord> edgeTiles;
        public List<Carving> connectedRooms;
        public int roomSize;
        public bool isAccessableFromMainRoom;
        public bool isMainRoom; 

        public Carving()
        {
        }

        public Carving(List<Coord> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Carving>();

            edgeTiles = new List<Coord>();
            foreach (Coord tile in tiles)
            {
                for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
                {
                    for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                    {
                        if (x == tile.tileX || y == tile.tileY)
                        {
                            if (map[x, y] == 1)
                            {
                                edgeTiles.Add(tile);
                            }
                        }
                    }
                }
            }
        }

        public void setAccessibleFromMainRoom()
        {
            if (!isAccessableFromMainRoom)
            {
                isAccessableFromMainRoom = true;
                foreach(Carving connectedRoom in connectedRooms)
                {
                    connectedRoom.setAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectRooms(Carving roomA, Carving roomB)
        {
            if (roomA.isAccessableFromMainRoom)
            {
                roomB.setAccessibleFromMainRoom();
            }
            else if (roomB.isAccessableFromMainRoom)
            {
                roomA.setAccessibleFromMainRoom();
            }
            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Carving otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }

        public int CompareTo(Carving otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }
    }

     public void setRoomType(Room r)
    {
        this.roomType = r;
    }

    public Room getRoomType()
    {
        return roomType;
    }

    public void registerWall(int x, int y)
    {
        this.wallTiles.Add(new Coord(x, y));
    }

    public void registerVeg(int x, int y)
    {
        this.vegRegion.Add(new Vector2(x, y));
    }

    public void registerFloor(int x, int y) {
        Vector2 coord = new Vector2(x, y);
        this.floorTiles.Add(coord);
        
    }

   
    /*
    public List<Vector2> getWallTiles()
    {
        return wallTiles;
    }
    */

    public List<Vector2> getFloorTiles()
    {
        return this.floorTiles;
    }

    public List<Vector2> getVegRegion()
    {
        return vegRegion;
    }

  

    public void sortTiles()
    {
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for(int y = 0; y < map.GetLength(1); y++)
            {
                if(map[x,y] == 1)
                {
                    this.registerWall(x, y);
                }

                if(map[x,y] == 0)
                {
                    this.registerFloor(x, y);
                    if (x >= 5 && x < map.GetLength(0) - 4 && y >= 5 && y < map.GetLength(1) - 4)
                    {
                        this.registerVeg(x, y);
                    }
                }

               

            }
        }
    }
    
}