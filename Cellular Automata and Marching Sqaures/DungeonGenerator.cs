﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DungeonGenerator : MonoBehaviour
{
    public MapPooler mapPooler;
    public ObjectPooler doorPooler;
    public TreePooler treePooler;
    public ObjectPooler teleportPooler;
    public WorldTeleporter entrance;
    public WorldTeleporter exit;
    public BoxCollider EnvironmentalTrigger;

    public int dungeonWidth = 6;
    public int dungeonHeight = 6;
    public int RoomSize = 128;
    public float cellSize = 5f;
    public string seed;
    public bool useRandomSeed;
    public bool useRandomBiome;
    public int vegGrowthPercentatage = 5;
    public int biomeNumber;
    public int progressionValue = 2;
    public GameObject[,] map;
    public MapGenerator[,] gens;
    public bool debugMode = false;
    private NewDoor[,] horizontalDoors;
    private NewDoor[,] verticalDoors;
    [SerializeField]
    int temperatureX;
    [SerializeField]
    int rainFallY;


    Dungeon dungeonLayout;
    public BiomeAssetPackage bap;

    [SerializeField]
    Transform doorRef;
    [SerializeField]
    Transform vegRef;

    System.Random pseudoRandom;

    PathRequestManager[,] pathRequestManagers;


    float accumulatedLoopTime = 0f;
    float accumInitRoomTime = 0f;
    float roomGenAccumTime = 0f;
    float vegetationGenerationAccumTime = 0f;
    float collisionGenerationAccumTime = 0f;




    void Update()
    {
        if (debugMode)
        {
            if (Input.GetMouseButtonDown(1))
            {
                clearMap();
                initialize();
                instantiateDungeon();
            }
        }
    }

    public void generateDungeon()
    {
        
        clearMap();
        initialize();
        instantiateDungeon();
    }

    public void initialize()
    {
        float initializeTimer = 0f;
        if (debugMode)
        {
            initializeTimer = Time.realtimeSinceStartup;
        }
        map = new GameObject[dungeonWidth, dungeonHeight];
        gens = new MapGenerator[dungeonWidth, dungeonHeight];
        pathRequestManagers = new PathRequestManager[dungeonWidth, dungeonHeight];

        if (useRandomSeed)
        {
            seed = generateSeedValue();
        }

        pseudoRandom = new System.Random(seed.GetHashCode());
        if (useRandomBiome)
        {
            temperatureX = pseudoRandom.Next(0,100);
            int maxRainfall = this.Isqrt(temperatureX * 100);
          //  Debug.Log("Max Rain Fall " + maxRainfall);
          //  Debug.Log("TemperatureX " + temperatureX);
           
            rainFallY = pseudoRandom.Next(0, maxRainfall + 1);
           // Debug.Log("Rain Fall " + rainFallY);
            bap = BiomeAssetManager.getInstance().getAssets((float)temperatureX / 100f, (float)rainFallY / 100f);
            
        }
        else
        {
            bap = BiomeAssetManager.getInstance().getAssets(biomeNumber);
        }
        // Debug.Log("is Bap Null? " + bap == null);
        if (!BiomeAssetManager.getInstance().environmentColorizer.isUsingDefaultColorPallet())
        {
            bap.applySkyColor(BiomeAssetManager.getInstance().environmentColorizer.getPickedPallet().skyColor);
            bap.applyChlorophyll(BiomeAssetManager.getInstance().environmentColorizer.getPickedPallet().chlorophyllColor);
        }
      

        EnvironmentalTrigger.GetComponent<EnvironmentTrigger>().biomeAssets = bap;
        this.treePooler = bap.getTreePooler();

        if (debugMode)
        {
            Debug.Log("Initizlization Time: " + (Time.realtimeSinceStartup - initializeTimer));
        }
        float triggerLocX = (float)RoomSize * (float)cellSize * ((float)((dungeonWidth - 1) / 2));
        float triggerLocY = (float)RoomSize * (float)cellSize * ((float)((dungeonHeight - 1) / 2));
        EnvironmentalTrigger.transform.localPosition = new Vector3(triggerLocX, 0f, triggerLocY);
        float triggerScaleX = (float)RoomSize * (float)cellSize * (float)(dungeonWidth);
        float triggerScaleY = (float)RoomSize * (float)cellSize * (float)(dungeonHeight);
        EnvironmentalTrigger.size = new Vector3(triggerScaleX, 200f, triggerScaleY);
    }

    public void instantiateDungeon()
    {
        float initialTime = 0f;
        if (debugMode)
        {
            initialTime = Time.realtimeSinceStartup;
        }
        

        dungeonLayout = new Dungeon(dungeonWidth, 1,pseudoRandom);

        
        // Debug.Log("Start "+Time.time);
        for (int i = 0; i < dungeonWidth; i++)
        {
            

            for(int j =0; j < dungeonHeight; j++)
            {
                this.generateRooms(i, j);
            }
            
        }
        placeDoors();

        if (debugMode)
        {

            Debug.Log("Average Room initialization Time " + (dungeonWidth * dungeonHeight) + " Rooms: " + accumInitRoomTime / (dungeonHeight * dungeonWidth));
            Debug.Log("Average Room Map & Geometry Generation Time " + (dungeonWidth * dungeonHeight) + " Rooms: " +  roomGenAccumTime/ (dungeonHeight * dungeonWidth));
            Debug.Log("Average Room Vegetation & Generation Time " + (dungeonWidth * dungeonHeight) + " Rooms: " + vegetationGenerationAccumTime / (dungeonHeight * dungeonWidth));
            Debug.Log("Average Room Collision Map Generation Time " + (dungeonWidth * dungeonHeight) + " Rooms: " + collisionGenerationAccumTime / (dungeonHeight * dungeonWidth));
            Debug.Log("Average Total Room Generation Time "+ (dungeonWidth*dungeonHeight) + " Rooms: " + accumulatedLoopTime / (dungeonHeight * dungeonWidth));
            Debug.Log("Generation Time: " + (Time.realtimeSinceStartup - initialTime));
        }
        //Debug.Log("Finish " + Time.time);
    }


    void generateRooms(int i , int j)
    {

        float loopStartTime = 0f;
        if (debugMode)
        {
            loopStartTime = Time.realtimeSinceStartup;
        }

        float roomInitializationStartTime = 0f;
        if (debugMode)
        {
            roomInitializationStartTime = Time.realtimeSinceStartup;
        }
        GameObject mapGen = mapPooler.getMapGenerator();
        mapGen.SetActive(true);
        mapGen.transform.position = transform.position + new Vector3(i * RoomSize * cellSize, 0f, j * RoomSize * cellSize);
        mapGen.transform.parent = transform;
        MapGenerator mg = mapGen.GetComponent<MapGenerator>();
        mg.setRoomType(dungeonLayout.getRoomLayout()[i, j]);
        mg.width = RoomSize;
        mg.height = RoomSize;
        mg.squareSize = cellSize;
        mg.useRandomSeed = false;
        mg.seed = generateSeedValue();
        int fillRate = pseudoRandom.Next(45, 60);
        mg.randomFillPercent = fillRate;
        mg.temp = temperatureX;
        mg.rain = rainFallY;
        mg.floor = bap.getFloorMaterial();
        mg.grass = new Material(bap.getGrass());
        mg.wall = bap.getWallMaterial();

        map[i, j] = mapGen;
        gens[i, j] = mg;


        float roomGenerationStartTime = 0f;

        if (debugMode)
        {
            accumInitRoomTime += (Time.realtimeSinceStartup - roomInitializationStartTime);
            roomGenerationStartTime = Time.realtimeSinceStartup;
        }


        StartCoroutine(generateMap(i, j));
        //  gens[i, j].GenerateMap();

        float vegetationGenerationStartTime = 0f;

        if (debugMode)
        {
            roomGenAccumTime += (Time.realtimeSinceStartup - roomGenerationStartTime);
            vegetationGenerationStartTime = Time.realtimeSinceStartup;
        }


        placeVegetation(gens[i, j]);

        float collisionGenerationStartTime = 0f;

        if (debugMode)
        {
            vegetationGenerationAccumTime += (Time.realtimeSinceStartup - vegetationGenerationStartTime);
            collisionGenerationStartTime = Time.realtimeSinceStartup;
        }


        mg.createCollisionMap();
        if (debugMode)
        {
            collisionGenerationAccumTime += (Time.realtimeSinceStartup - collisionGenerationStartTime);
        }

        pathRequestManagers[i, j] = mg.gameObject.GetComponentInChildren<PathRequestManager>();

        if (debugMode)
        {
            accumulatedLoopTime += (Time.realtimeSinceStartup - loopStartTime);
        }
    }


    void placeVegetation(MapGenerator mg)
    {
        
        int treeLimit = 35;
        int treeCount = 0;
        List<Vector2> coords = mg.getVegRegion();
     //   Debug.Log("VEG COUNT" + coords.Count);
       



        float growthRate = bap.getTreeDensity();

        for(int i = 0; i < treeLimit + 1; i++)
        {
            int rtd = pseudoRandom.Next(0, coords.Count);
            int percentChance = (int)((growthRate*coords.Count));
            if (rtd < percentChance)
            {
                int pick = pseudoRandom.Next(0, coords.Count);
                Vector2 coord = coords[pick];
                coords.Remove(coord);

                int treePick = pseudoRandom.Next(0, treePooler.getNumberOfPooledObjects());
                GameObject tree = treePooler.getObject(treePick);
                Vector3 pos = mg.CoordToWorldPoint((int) coord.x,(int) coord.y) - new Vector3(0f, 3f*cellSize, 0f);
                int rotY = pseudoRandom.Next(0, 360);
                tree.transform.rotation = Quaternion.Euler(0f, (float)rotY, 0f);
                tree.SetActive(true);
                tree.transform.position = pos;
                tree.transform.parent = vegRef;
                
                treeCount++;
            }
        }
    }

    public void placeDungeonTeleporter(bool entranceOrExit)
    {
        GameObject teleporter = teleportPooler.getObject();
        teleporter.SetActive(true);
        teleporter.transform.localScale = new Vector3(cellSize,cellSize, cellSize);
        int row;
        if (!entranceOrExit)
        {
             row = dungeonWidth - 1;
             this.exit = teleporter.GetComponent<WorldTeleporter>();
        }
        else{
             row = 0;
             this.entrance = teleporter.GetComponent<WorldTeleporter>();
        }
        int column = this.pseudoRandom.Next(0, dungeonHeight - 1);
        MapGenerator mg = gens[row, column];
        List<Vector2> floorTiles = mg.getVegRegion();
        int pick = pseudoRandom.Next(0, floorTiles.Count);
        Vector2 placementCoord = floorTiles[pick];
        Vector3 pos = mg.CoordToWorldPoint((int)placementCoord.x, (int)placementCoord.y) - new Vector3(0f, 3f * cellSize, 0f);
        teleporter.transform.position = pos;
        teleporter.transform.parent = doorRef;
        teleporter.transform.localEulerAngles = Vector3.zero;
        

    }

    public void setDestinationOfTeleporter(bool entranceOrExit,Transform destLoc,GameObject host, GameObject destination)
    {
        if (!entranceOrExit)
        {
            if (exit != null)
            {
                exit.setTPDestination(destLoc, host, destination);
            }
        }

        else
        {
            if (entrance != null)
            {
                entrance.setTPDestination(destLoc, host, destination);
            }
        }
    }

    void placeDoors()
    {
        horizontalDoors = new NewDoor[dungeonWidth , dungeonHeight -1];
        verticalDoors = new NewDoor[dungeonWidth , dungeonHeight];

        float horizontal = ((float)RoomSize * (float)cellSize)/2f;

        for(int i = 0; i < dungeonWidth; i++)
        {
            for(int j = 0; j < dungeonHeight - 1; j++)
            {
                if(gens[i,j].getRoomType() is TypeOneRoom || gens[i,j].getRoomType() is TypeTwoRoom || gens[i, j].getRoomType() is TypeThreeRoom || gens[i,j].getRoomType() is TypeFourRoom)
                {
                    GameObject door = doorPooler.getObject();
                    door.SetActive(true);
                    door.transform.localScale = new Vector3(cellSize, 2f * cellSize,cellSize);
                    door.transform.position = gens[i, j].transform.position + new Vector3(0f, -2f*cellSize, horizontal);
                    door.transform.parent = doorRef;
                    NewDoor nd = door.GetComponent<NewDoor>();
                    horizontalDoors[i, j] = nd;
                }
            }
        }

        for (int i = 0; i < dungeonWidth; i++)
        {
            for (int j = 0; j < dungeonHeight; j++)
            {
                if (gens[i, j].getRoomType() is TypeThreeRoom || gens[i, j].getRoomType() is TypeFourRoom)
                {
                    GameObject door = doorPooler.getObject();
                    door.SetActive(true);
                    door.transform.localScale = new Vector3(cellSize, 2f * cellSize,cellSize);
                    door.transform.position = gens[i, j].transform.position + new Vector3(-horizontal, -2f*cellSize, 0f);
                    door.transform.rotation = Quaternion.Euler(90, 90, 0);
                    door.transform.parent = doorRef;
                    NewDoor nd = door.GetComponent<NewDoor>();
                    verticalDoors[i, j] = nd;
                }
            }
        }

        this.lockDoors(progressionValue);
    }

    void lockDoors(int progNumber)
    {
        int keyRow = pseudoRandom.Next(1, this.dungeonHeight-1);
        for(int i = 0; i < verticalDoors.GetLength(1); i++)
        {
            if(verticalDoors[keyRow,i] != null)
            {
                verticalDoors[keyRow, i].lockDoor(progNumber);
            }
        }
 
        for (int i = 0; i < keyRow; i++)
        {
            for(int j = 0; j < verticalDoors.GetLength(1); j++)
            {
               
                if (verticalDoors[i, j] != null)
                {
                    int lockNum = pseudoRandom.Next(1,progNumber);
                    verticalDoors[i, j].lockDoor(lockNum);
                }
            }
        }
        
        for (int i = keyRow+1; i < verticalDoors.GetLength(0); i++)
        {
            for (int j = 0; j < verticalDoors.GetLength(1); j++)
            {
                if (verticalDoors[i, j] != null)
                {
                    int lockNum = pseudoRandom.Next(1, progNumber+1);
                    verticalDoors[i, j].lockDoor(lockNum);
                }
            }
        }

        for (int i = 0; i < keyRow; i++)
        {
            for (int j = 0; j < horizontalDoors.GetLength(1); j++)
            {

                if (horizontalDoors[i, j] != null)
                {
                    int lockNum = pseudoRandom.Next(1, progNumber);
                    horizontalDoors[i, j].lockDoor(lockNum);
                }
            }
        }

        for (int i = keyRow; i < horizontalDoors.GetLength(0); i++)
        {
            for (int j = 0; j < horizontalDoors.GetLength(1); j++)
            {
                if (horizontalDoors[i, j] != null)
                {
                    int lockNum = pseudoRandom.Next(1, progNumber + 1);
                    horizontalDoors[i, j].lockDoor(lockNum);
                }
            }
        }

    }

    public string generateSeedValue()
    {
        string lib = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890`~!@$%^&*-_+=?/|";
        string seedPack = "";
        for (int i = 0; i < 25; i++)
        {
            int index = UnityEngine.Random.Range(0, lib.Length);
            seedPack += lib[index];
        }

        return seedPack;
    }

    IEnumerator generateMap(int x, int y)
    {
        gens[x, y].GenerateMap();
        yield return new WaitForSeconds(1f);
        
    }



    void clearMap()
    {
        if (map != null)
        {
            for (int i = 0; i < dungeonWidth; i++)
            {
                for (int j = 0; j < dungeonHeight; j++)

                {
                    map[i, j].transform.parent = null;
                    map[i, j].SetActive(false);
                }
            }

            int doorCount = doorRef.childCount;
            List<Transform> doors = new List<Transform>();
            for (int i = 0; i < doorCount; i++)
            {
                doors.Add(doorRef.GetChild(i));
            }

            foreach (Transform d in doors)
            {
                d.parent = null;
                d.gameObject.SetActive(false);
            }

            int treeCount = vegRef.childCount;
            List<Transform> trees = new List<Transform>();
            for (int i = 0; i < treeCount; i++)
            {
                trees.Add(vegRef.GetChild(i));
            }

            foreach (Transform t in trees)
            {
                t.parent = null;
                t.gameObject.SetActive(false);
            }

        }
    }

    public void requestPath(int roomX,int roomY, Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        pathRequestManagers[roomX,roomY].RequestPath(pathStart, pathEnd, callback);
    }

    int Isqrt(int num)
    {
        if (0 == num) { return 0; }  // Avoid zero divide  
        int n = (num / 2) + 1;       // Initial estimate, never low  
        int n1 = (n + (num / n)) / 2;
        while (n1 < n)
        {
            n = n1;
            n1 = (n + (num / n)) / 2;
        } // end while  
        return n;
    } // end Isqrt()  

    

}