﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class WorldGenerator : MonoBehaviour {

    public List<DungeonGenerator> dunGens;
    public bool debugMode = false;
    public GameObject MainBase;
    public WorldTeleporter MainBaseEntrance;
    public WorldTeleporter MainBaseExit;
    
    public Color[] lightingColors;
    public Color[] chlorophyllColors;
    public Color[] skyColors;
    public bool useRandomColorScheme = false;
    

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(1))
        {
            BiomeAssetManager.getInstance().resetAssets();
            if (useRandomColorScheme)
            {
                BiomeAssetManager.getInstance().environmentColorizer.pickColorPallete(Time.realtimeSinceStartup.ToString(), false);
                BiomeAssetManager.getInstance().environmentColorizer.applyGlobalSkyColors();
            }
            else
            {
                BiomeAssetManager.getInstance().environmentColorizer.pickColorPallete(Time.realtimeSinceStartup.ToString(), true);
                BiomeAssetManager.getInstance().environmentColorizer.applyGlobalSkyColors();
            }
            if (debugMode)
            {
                for (int i = 0; i < dunGens.Count; i++)
                {
                    dunGens[i].gameObject.SetActive(true);
                }

                for (int i = 0; i < dunGens.Count; i++)
                {
                    

                    dunGens[i].generateDungeon();
                    
                    dunGens[i].placeDungeonTeleporter(true);
                    dunGens[i].placeDungeonTeleporter(false);
                    if (i > 0)
                    {
                        GameObject host = dunGens[i].gameObject;
                        GameObject destination = dunGens[i - 1].gameObject;
                        dunGens[i].setDestinationOfTeleporter(true,dunGens[i - 1].exit.destTransform, host, destination);
                        dunGens[i - 1].setDestinationOfTeleporter(false,dunGens[i].entrance.destTransform, destination, host);
                    }
                }

                MainBaseEntrance.setTPDestination(dunGens[0].entrance.destTransform, MainBase, dunGens[0].gameObject);
                dunGens[0].setDestinationOfTeleporter(true, MainBaseEntrance.destTransform,dunGens[0].gameObject, MainBase);
                MainBaseExit.setTPDestination(dunGens[dunGens.Count - 1].exit.destTransform, MainBase, dunGens[dunGens.Count - 1].gameObject);
                dunGens[dunGens.Count - 1].setDestinationOfTeleporter(false, MainBaseExit.destTransform, dunGens[dunGens.Count - 1].gameObject, MainBase);
                for (int i = 0; i < dunGens.Count; i++)
                {
                    dunGens[i].gameObject.SetActive(false);
                }
            }
        }
	}

    void OnApplicationQuit()
    {
        Debug.Log("Application ending after " + Time.time + " seconds");
        BiomeAssetManager.getInstance().resetAssets();
    }
}
