﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]


public class GroundFloor : MonoBehaviour {
    //number of tiles in the x direction
    [SerializeField]
    private int size_x = 35;
    //number of tiles in the z direction
    [SerializeField]
    private int size_z = 35;

    //size of each tile
    [SerializeField]
    private float tileSize = 10f;
    [SerializeField]
    private bool recalculatenormals = true;
    [SerializeField]
    private float minimumHeight = -5f;
    [SerializeField]
    private float maximumHeight = 5f;
    private float[,] heightMap;
    [SerializeField]
    private Color minimumGroundColor;
    [SerializeField]
    private Color hullColor;
    [SerializeField]
    private float colorBias;
    [SerializeField]
    private GameObject grassLayer;
    public bool genGrass = true;
    private int temperature;
    private int rainfall;

    public void buildMesh(int _x, int _y, float _tileSize, Material floor, Material grass, int temp, int rain)
    {

        this.size_x = _x;
        this.size_z = _y;
        this.tileSize = _tileSize;
        this.temperature = temp;
        this.rainfall = rain;

        //number of tiles total on the map
        int numTiles = size_x * size_z;
        //number of triangles is double the number of tiles needed as 2 triangles make one square on the map.
        int numTris = numTiles * 2;

        //number of vertices in the x dirextion
        int vsize_x = size_x + 1;
        //number of vertices in the z direction
        int vsize_z = size_z + 1;
        // number of vertices total in the map needed to make the plane.
        int numVerts = vsize_x * vsize_z;


        //Create mesh data.
        //array of vector 3's to store vertex data
        Vector3[] vertices = new Vector3[numVerts];
        //array of vector 3's to store normal data
        Vector3[] normals = new Vector3[numVerts];
        //array of vector 2's to store uv data.
        Vector2[] uv = new Vector2[numVerts];

        int[] triangles = new int[numTris * 3];
        //One nested for loop to set up the vertices normals, and uv
        int x, z;
        for (z = 0; z < vsize_z; z++)
        {
            for (x = 0; x < vsize_x; x++)
            {
                vertices[z * vsize_x + x] = new Vector3(x * tileSize, 0f, z * tileSize);
                normals[z * vsize_x + x] = Vector3.up;
                uv[z * vsize_x + x] = new Vector2((float)x / size_x, (float)z / size_z);

            }
        }

        this.applyHeightData(vertices);


        for (z = 0; z < size_z; z++)
        {
            for (x = 0; x < size_x; x++)
            {
                int squareIndex = z * size_x + x;
                int triOffset = squareIndex * 6;

                triangles[triOffset + 0] = z * vsize_x + x + 0;
                triangles[triOffset + 2] = z * vsize_x + x + vsize_x + 1;
                triangles[triOffset + 1] = z * vsize_x + x + vsize_x + 0;

                triangles[triOffset + 3] = z * vsize_x + x + 0;
                triangles[triOffset + 5] = z * vsize_x + x + 1;
                triangles[triOffset + 4] = z * vsize_x + x + vsize_x + 1;

            }
        }

        //Create a new mesh
        Mesh mesh = new Mesh();
        //Populate mesh with data
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.normals = normals;
        mesh.uv = uv;
        //Assign our mesh to our filter/renderer/collider.
        MeshFilter mesh_filter = GetComponent<MeshFilter>();
        MeshRenderer mesh_renderer = GetComponent < MeshRenderer > ();
        mesh_renderer.sharedMaterial = floor;
        mesh_renderer.sharedMaterial.mainTextureScale = new Vector2(size_x / 2, size_z / 2);

        MeshCollider mesh_collider = GetComponent<MeshCollider>();

        if (recalculatenormals)
        {
            mesh.RecalculateNormals();
        }
        this.calculateMeshTangents(mesh);
        mesh_filter.mesh = mesh;
        //mesh_collider.sharedMesh  = mesh;
        mesh_collider.sharedMesh = mesh_filter.sharedMesh;
       



        Mesh m = mesh_filter.mesh;
       
        grassLayer.SetActive(true);
        grassLayer.GetComponent<MeshFilter>().mesh = m;
        if (genGrass)
        {
            this.makeGrassMap(grass, temperature, rainfall);
        }
       
    }


    public float[,] makeHeightData()
    {
        MapGenerator mapgen = transform.parent.GetComponent<MapGenerator>();
        string seed = mapgen.seed;
        System.Random pseudoRandom = new System.Random(seed.GetHashCode());
        

        //number of vertices in the x dirextion
        int vsize_x = size_x + 1;
        //number of vertices in the z direction
        int vsize_z = size_z + 1;
        float height;
        float[,] heightMap = new float[vsize_x, vsize_z];

        for (int z = 0; z < vsize_z; z = z + 2)
        {
            for (int x = 0; x < vsize_x; x = x + 2)
            {
                //if (z == 0 || x == 0 || z == vsize_z - 1 || x == vsize_x - 1 || z == 2 || x == 2 || z == vsize_z - 3 || x == vsize_x - 3 || z == 4 || x == 4 || x == vsize_x - 5 || z == vsize_z - 5)
                if (z == 0 || x == 0  || z == 2 || x == 2 || z == 4 || x == 4 || z == vsize_z - 1 || x == vsize_x - 1 || z == vsize_z - 3 || x == vsize_x - 3 || x == vsize_x - 5 || z == vsize_z - 5 || x == vsize_x - 6 || z == vsize_z - 6)
                {
                   // Debug.Log("X is " + x + "Z is "+z);
                    
                    height = 0f;
                }
                else
                {
                  
                    int s = pseudoRandom.Next(-100, 100);
                    if (s <= 0)
                    {
                        height = minimumHeight * ((float)s / (float)-100); 
                    }
                    else
                    {
                        height = maximumHeight * ((float)s / (float)100);
                    }

                }
                if (z < vsize_z && x < vsize_x)
                {
                    heightMap[x, z] = height;
                }

                if (z + 1 < vsize_z && x + 1 < vsize_x)
                {
                    heightMap[x + 1, z] = height;
                    heightMap[x, z + 1] = height;
                    heightMap[x + 1, z + 1] = height;
                }

            }
        }
        this.heightMap = heightMap;
        return heightMap;
    }

    public float getHeightFromCoordinate(int x, int y)
    {
        return this.heightMap[x + 1, y];
    }

    public float[,] getHeightMapData()
    {
        return this.heightMap;
    }

    public void applyHeightData(Vector3[] vertices)
    {
        //number of vertices in the x dirextion
        int vsize_x = size_x + 1;
        //number of vertices in the z direction
        int vsize_z = size_z + 1;

        float[,] heightData = this.makeHeightData();
        for (int z = 0; z < vsize_z; z++)
        {
            for (int x = 0; x < vsize_x; x++)
            {
                vertices[z * vsize_x + x] += new Vector3(0f, heightData[x, z], 0f);
            }
        }
    }

    public void calculateMeshTangents(Mesh mesh)
    {
        //speed up math by copying the mesh arrays
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        Vector2[] uv = mesh.uv;
        Vector3[] normals = mesh.normals;

        //variable definitions
        int triangleCount = triangles.Length;
        int vertexCount = vertices.Length;

        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];

        Vector4[] tangents = new Vector4[vertexCount];

        for (long a = 0; a < triangleCount; a += 3)
        {
            long i1 = triangles[a + 0];
            long i2 = triangles[a + 1];
            long i3 = triangles[a + 2];

            Vector3 v1 = vertices[i1];
            Vector3 v2 = vertices[i2];
            Vector3 v3 = vertices[i3];

            Vector2 w1 = uv[i1];
            Vector2 w2 = uv[i2];
            Vector2 w3 = uv[i3];

            float x1 = v2.x - v1.x;
            float x2 = v3.x - v1.x;
            float y1 = v2.y - v1.y;
            float y2 = v3.y - v1.y;
            float z1 = v2.z - v1.z;
            float z2 = v3.z - v1.z;

            float s1 = w2.x - w1.x;
            float s2 = w3.x - w1.x;
            float t1 = w2.y - w1.y;
            float t2 = w3.y - w1.y;

            float r = 1.0f / (s1 * t2 - s2 * t1);

            Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
            Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;

            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }


        for (long a = 0; a < vertexCount; ++a)
        {
            Vector3 n = normals[a];
            Vector3 t = tan1[a];

            Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        mesh.tangents = tangents;
    }

    void makeGrassMap(Material grass, int temp, int rain)
    {
        int tempVal = temp;
        int rainVal = rain;
     //   Debug.Log("temp " + temp);
     //   Debug.Log("rain " + rain);
        int maxDensity = 200;
        int grassGrowth = tempVal + rainVal;
        Debug.Log("grassGrowth " + grassGrowth);
        if (grassGrowth > maxDensity)
        {
            grassGrowth = maxDensity;
        }

        MapGenerator mapgen = transform.parent.GetComponent<MapGenerator>();
        string seed = mapgen.seed;
        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        
        int textWidth = size_x;
        int textHeight = size_z;
        Texture2D texture = new Texture2D(textWidth, textHeight);
        int[,] map = mapgen.map;
        for(int i = 0; i < map.GetLength(0); i++)
        {
            for(int j = 0; j < map.GetLength(1); j++)
            {
                if(map[i,j] == 1)
                {
                    texture.SetPixel(i, j, Color.black);
                }
                else
                {
                    int p = pseudoRandom.Next(0,grassGrowth);
                    float density = (float)p / (float)maxDensity;
                    Color c = new Color(density,0f,0f,0f);
                    texture.SetPixel(i, j, c);
                }
            }
        }

        texture.filterMode = FilterMode.Point;
        texture.Apply();

        MeshRenderer mesh_renderer = grassLayer.GetComponent<MeshRenderer>();
        mesh_renderer.sharedMaterial = grass;
        mesh_renderer.sharedMaterial.SetTexture("_Density", texture);


    }

   

}
