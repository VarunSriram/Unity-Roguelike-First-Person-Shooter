﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BiomeAssetManager : MonoBehaviour {

    private static BiomeAssetManager instance;

    [SerializeField]
    private TreePooler[] treePoolers = new TreePooler[8];
    [SerializeField]
    private Material[] floors = new Material[8];
    [SerializeField]
    private Material[] grass = new Material[8];
    [SerializeField]
    private Material[] walls = new Material[8];
    [SerializeField]
    private Material[] skyBoxes = new Material[8];

    [SerializeField]
    private float[] treeDensitys = { 0.25f, 0.35f, 0.25f, 0.535f, 0.535f, 0.45f, 0.735f, 0.85f };
    [SerializeField]
    private List<BiomeAssetPackage> usedBiomeAssets;

    



    private Dictionary<int , BiomeAssetPackage > assetDictionary;
    public EnvironmentColorizer environmentColorizer;

    public static float
      tundraTempMin = 0f,
      savannahTempMin = 0.4f,
      desertTempMin = 0.75f,
      taigaTempMin = 0.0625f,
      forestTempMin = 0.4f,
      plainsTempMin = 0.75f,
      swampTempMin = 0.4f,
      rainforestTempMin = 0.75f,

       tundraTempMax = 0.4f,
      savannahTempMax = 0.75f,
      desertTempMax = 1.0f,
      taigaTempMax = 0.4f,
      forestTempMax = 0.75f,
      plainsTempMax = 1.0f,
      swampTempMax = 0.75f,
      rainforestTempMax = 1.0f,

      tundraRainMin = 0.0f,
      savannahRainMin = 0.0f,
      desertRainMin = 0.0f,
      taigaRainMin = 0.25f,
      forestRainMin = 0.25f,
      plainsRainMin = 0.25f,
      swampRainMin = 0.55f,
      rainforestRainMin = 0.55f,

      tundraRainMax = 0.25f,
      savannahRainMax = 0.25f,
      desertRainMax = 0.25f,
      taigaRainMax = 0.6325f,
      forestRainMax = 0.55f,
      plainsRainMax = 0.55f,
      swampRainMax = 0.866f,
      rainforestRainMax = 1.0f;




    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

   

    // Use this for initialization
    void Start() {
        assetDictionary = new Dictionary<int, BiomeAssetPackage>();
        usedBiomeAssets = new List<BiomeAssetPackage>();
        for (int i = 0; i < 8; i++)
        {
            assetDictionary.Add(i , new BiomeAssetPackage(treePoolers[i], walls[i], floors[i], skyBoxes[i],grass[i],treeDensitys[i]));
        }

        environmentColorizer = GetComponent<EnvironmentColorizer>();

	}

    public static  BiomeAssetManager getInstance()
    {
        return instance;
    }

    public BiomeAssetPackage getAssets(int key)
    {
        if (assetDictionary.ContainsKey(key-1)){
            usedBiomeAssets.Add(assetDictionary[key-1]);
            Debug.Log(usedBiomeAssets.Count);
            return assetDictionary[key-1];
        }
        else
        {
            return null;
        }
        
    }

    public void resetAssets()
    {
        if (usedBiomeAssets.Count > 0)
        {
            environmentColorizer.resetAssets(usedBiomeAssets);
            usedBiomeAssets.Clear();
        }
    }


    

    public EnvironmentColorizer getEnvironmentColorizer()
    {
        return this.environmentColorizer;
    }

    public BiomeAssetPackage getAssets(Biome b)
    {
        if (b is Tundra)
        {
            return getAssets(1);
        }
        else if (b is Savanna)
        {
            return getAssets(2);
        }
        else if (b is Desert)
        {
            return getAssets(3);
        }
        else if (b is Taiga)
        {
            return getAssets(4);
        }
        else if (b is Forest)
        {
            return getAssets(5);
        }
        else if (b is Plains)
        {
            return getAssets(6);
        }
        else if (b is Swamp)
        {
            return getAssets(7);
        }
        else if (b is RainForest)
        {
            return getAssets(8);
        }
        else {
            return getAssets(1);
        }
    }

    public BiomeAssetPackage getAssets(float temperatureX, float rainFallY )
    {
        if (temperatureX < Biome.tundraTempMax && rainFallY < Biome.tundraRainMax)
        {
            return getAssets(1);
        }

        else if ((temperatureX >= Biome.savannahTempMin && temperatureX < Biome.savannahTempMax) && (rainFallY < Biome.savannahRainMax))
        {
            return getAssets(2);
        }

        //If the temperature is greater than 0.75 and rainfall is less than 0.25 then the biome of this world is a Desert.
        else if (temperatureX >=Biome.desertTempMin && rainFallY <= Biome.desertRainMax)
        {
            return getAssets(3);
        }
        //If the temperature is greater than 0.0625 and 0.4 and the rainfall is between 0.25 and 0.6325 then the biome of this world is a Taiga.
        else if ((temperatureX > Biome.taigaTempMin && temperatureX < Biome.taigaTempMax) && (rainFallY >= Biome.taigaRainMin && rainFallY < Biome.taigaRainMax))
        {
            return getAssets(4);
        }
        //If the temperature is between 0.4 and 0.75 and the rain fall is between 0.25 and 0.55 then the biome of this world is a temporal forest.
        else if ((temperatureX >= Biome.forestTempMin && temperatureX < Biome.forestTempMax) && (rainFallY >= Biome.forestRainMin && rainFallY < Biome.forestRainMax))
        {
            return getAssets(5);
        }
        //IF the temperature is betwee 0.75 and 1.0 and rainfall is between 0.25 and 0.55 then the biome of this world is plain.
        else if ((temperatureX >= Biome.plainsTempMin && temperatureX <= Biome.plainsTempMax) && (rainFallY >= Biome.plainsRainMin && rainFallY < Biome.plainsRainMax))
        {
            return getAssets(6);
        }
        //If the temperature is between 0.4 and 0.75 and the rain fall is between 0.55 amd 0.866 then the biome of this world is a swamp.
        else if ((temperatureX >= Biome.swampTempMin && temperatureX < Biome.swampTempMax) && (rainFallY >= Biome.swampRainMin && rainFallY < Biome.swampRainMax))
        {
            return getAssets(7);
        }
        //If the temperature is between 0.75 and 1.0 and the rain fall is between 0.55 and 1.0 them the biome of this world is a rainforest.
        else if ((temperatureX >= Biome.rainforestTempMin && temperatureX <= Biome.rainforestTempMax) && (rainFallY >= Biome.rainforestRainMin && rainFallY < Biome.rainforestRainMax))
        {
            return getAssets(8);
        }
        else
        {
            return getAssets(1);
        }
    }

    
   
}
