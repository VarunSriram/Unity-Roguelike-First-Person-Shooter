﻿using UnityEngine;
using System.Collections;

public class BiomeAssetPackage {
    private TreePooler treePool;
    private Material walls;
    private Material floor;
    private Material skyBox;
    private Material grass;
    private float treeDensity;

    private Color defaultSkyColor;
    private Color defaultChlorophyllColor;
    private Color defaultGrassSpecularColor;
    private Color defaultGrassSecondaryColor;

    private Color specGrassColor = new Color(0.1f, 0.1f, 0.1f, 1f);

    public BiomeAssetPackage(TreePooler treePool, Material walls,Material floor, Material skyBox, Material grass,float treeDensity)
    {
        this.treePool = treePool;
        this.walls = walls;
        this.floor = floor;
        this.skyBox = skyBox;
        this.grass = grass;
        this.treeDensity = treeDensity; 
        this.defaultSkyColor = this.skyBox.GetColor("_SkyTint");
        this.defaultChlorophyllColor = this.grass.GetColor("_Color00");
        this.defaultGrassSecondaryColor = this.grass.GetColor("_SecColor00");
        this.defaultGrassSpecularColor = this.grass.GetColor("_SpecColor00");
    }

    public TreePooler getTreePooler()
    {
        return treePool;
    }

    public Material getWallMaterial()
    {
        return walls;
    }

    public Material getFloorMaterial()
    {
        return floor;
    }

    public Material getSkyBox()
    {
        return skyBox;
    }

    public Material getGrass()
    {
        return grass;
    }

    public float getTreeDensity()
    {
        return treeDensity;
    }

    public void resetMaterials()
    {
        this.grass.SetColor("_Color00", this.defaultChlorophyllColor);
        this.grass.SetColor("_SecColor00", this.defaultChlorophyllColor);
        this.grass.SetColor("_SpecColor00", this.defaultGrassSpecularColor);
        this.skyBox.SetColor("_SkyTint", this.defaultSkyColor);
        foreach (GameObject go in treePool.pooledObjects)
        {
            go.GetComponent<SpeedTreeMaterialManager>().restoreMaterials();
        }
    }

    public void applyChlorophyll(Color chloroColor)
    {
        this.grass.SetColor("_Color00", chloroColor);
        this.grass.SetColor("_SecColor00", chloroColor + specGrassColor);
        this.grass.SetColor("_SpecColor00", Color.black);
        foreach (GameObject go in treePool.pooledObjects)
        {
            go.GetComponent<SpeedTreeMaterialManager>().setLeafColor(chloroColor);
        }
    }

    public void applySkyColor(Color sky)
    {
        this.skyBox.SetColor("_SkyTint", sky);
    }
    
     
   
	
}
