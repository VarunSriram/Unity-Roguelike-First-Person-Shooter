﻿using UnityEngine;
using System.Collections;

public class TransformTeleporter : MonoBehaviour {
    public Transform dest;

    public virtual void OnTriggerEnter(Collider other)
    {
        if(other.tag == "MainCamera")
        {
            other.transform.position = dest.position;
        }
    }
	
}
