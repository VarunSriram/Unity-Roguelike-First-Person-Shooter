﻿using UnityEngine;
using System.Collections;

public class HidingTransformTeleporter : TransformTeleporter {
    public GameObject hostWorld;
    public GameObject destinationWorld;
	

    public override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera")
        {

            if (destinationWorld != null)
            {
                destinationWorld.SetActive(true);
            }
            other.transform.position = dest.position;
            if (hostWorld != null)
            {
                hostWorld.SetActive(false);
            }
        }
    }
}
