﻿using UnityEngine;
using System.Collections;
//Interface representing a common game character's functionality
public interface Character{
    //A character can move
	void onMove(); 
    //A character can sprint
	void onSprint();
    //A character can die
	void onDeath();
    //A character can jump
	void onJump();
    //A character can dodge
	void onDodge();
}
