﻿using UnityEngine;
using System.Collections;
//A script to represent the player character and his/her actons.
public class Player :  CharacterBase {
    private float staminaRegen;
	private float healthRegen;
	[SerializeField]
	private float sprintCost = 1f;
	[SerializeField]
	private float JumpCost = 600f;
	[SerializeField]
	private float DodgeCost = 375f;
	[SerializeField]
	private float ChargeAttackCost = 1000f;
	[SerializeField]
	private float minimumRunStamina = 250f;
	[SerializeField]
	private bool canRegenStamina = true;
    [SerializeField]
    private float walkSpeed = 17.5f;
    private float baseWalkSpeed;
    private float cachedStaminaRegen;

    //Inherited start method
	protected override  void Start () {
        //setup base stats specific to a player
        //this.stats = new CharacterStats(1500f, 1500f, 10f,1f, 2000f, 2000f, 5f, 0.015f,1f);
        this.stats = GetComponent<CharacterStats>();
        //get the stamina regen rate of the player
		staminaRegen = this.stats.getStaminaRegenRate ();
        //get the health regen rate of the player
		healthRegen = this.stats.getHealthRegenRate ();
        //Invoke health and stamina regen
		InvokeRepeating ("regenHealthStat", healthRegen, healthRegen);
		InvokeRepeating ("regenStaminaStat", staminaRegen, staminaRegen);
        //Register the player stats to the Player HUD
	//	GameObject.FindGameObjectWithTag ("MainUI").GetComponent<PlayerUIController> ().setPlayerModel (this.stats);
        this.baseWalkSpeed = walkSpeed;
	}
	//method to regen player health
	private void regenHealthStat(){
		this.stats.regenHealth ();
	}
    //method to regen player stamina
	private void regenStaminaStat(){
		if (canRegenStamina) {
			this.stats.regenStamina ();
		}
	}
    //Player can sprint
	public override void onSprint(){
        //spend stamina based on the sprint cost 
		this.stats.spendStamina (sprintCost);
	}
    //Player can die
	public override void onDeath(){
		Debug.Log ("I am Dead");
	}
    //Player can jump
	public override void onJump(){
        //spend stamina based on the jumpcost
		this.stats.spendStamina (JumpCost);
	}
    //Player can dodge
	public override void onDodge(){
        //halt the stamina regen
		this.canRegenStamina = false;
        //spend stamina based on dodge cost
		this.stats.spendStamina (DodgeCost);
        //resume the stamina regen.
		this.canRegenStamina = true;
	}
    //method for the player's charge attack
	public void chargeAttack(){
        //spend the stamina based on the charge attack cost.
		this.stats.spendStamina (this.ChargeAttackCost);
	}
    //method to toggle stamina regen status
	public void setStaminaRegenStatus(bool b){
		this.canRegenStamina = b;
	}
	//method to get the dodge cost
	public float getDodgeCost(){
		return this.DodgeCost;
	}
    //method to set the dodge cost
	public void setDodgeCost(float f){
		this.DodgeCost = f;
	}
    //method to get the jump cost
	public float getJumpCost(){
		return this.JumpCost;
	}
    //method to set the jump cost
	public void setJumpCost(float f){
		this.JumpCost = f;
	}
    //method to get sprint cost
	public float getSprintCost(){
		return this.sprintCost;
	}
    //method to get the minimum run stamina cost
	public float getMinimumRunStamina(){
		return this.minimumRunStamina;
	}
    //method set the sprint cost
	public void setSprintCost(float f){
		this.sprintCost = f;
	}
    //method to get the charge attack cost
	public float getChargeAttackCost(){
		return this.ChargeAttackCost;
	}
    //method to set the charge attack cost.
	public void setChargeAttackCost(float f){
		this.ChargeAttackCost = f;
	}

    public bool canChargeAttack()
    {
        return this.getCharacterStats().getCurrentStamina() >= this.ChargeAttackCost;
    }

    public bool canJump()
    {
        return this.getCharacterStats().getCurrentStamina() >= this.JumpCost;
    }

    public bool canDodge()
    {
        return this.getCharacterStats().getCurrentStamina() >= this.DodgeCost;
    }

    public bool isTired()
    {
        return this.getCharacterStats().getCurrentStamina() <= minimumRunStamina;
    }

    public void setStaminaRegenFlag(bool b)
    {
        this.canRegenStamina = b;
    }

    public void setWalkSpeed(float f) {
        this.walkSpeed = f;
    }

    public float getWalkSpeed()
    {
        return this.walkSpeed;
    }

    public void restoreWalkSpeed()
    {
        this.walkSpeed = this.baseWalkSpeed;
    }

   

}
