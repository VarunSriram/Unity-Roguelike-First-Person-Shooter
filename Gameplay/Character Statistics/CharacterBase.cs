﻿using UnityEngine;
using System.Collections;
//Class representing a basic game Character.
public class CharacterBase : MonoBehaviour , Character{
	//[SerializeField]
    //A character has character stats.
	public CharacterStats stats;

	protected virtual void Start () {
        //initialize base stats on startup
		stats = new CharacterStats(1000f, 1000f, 10f, 1f, 1500f, 1500f, 10f,1f, 1f);
	}
	
	// Update is called once per frame

    //method to get the stats of a particular character.
	public CharacterStats getCharacterStats(){
		return this.stats;
	}

    //none of the methods from character are implemented here however they will be inherited by other sub classes.

    public virtual void onMove()
    {

    }
    public virtual void onSprint()
    {

    }

    public virtual void onDeath()
    {

    }


    public virtual void onJump(){

	}
	public virtual void onDodge(){
		
	}
}
