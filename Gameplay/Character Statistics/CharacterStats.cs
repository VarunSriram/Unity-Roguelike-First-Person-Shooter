﻿using UnityEngine;
using System.Collections;
//Library to represent the the various stats common to all characters.
public class CharacterStats :MonoBehaviour{
	[SerializeField]
	private float currentHealth; //Current health of the character 
	[SerializeField]
	private float maxHealth; //maximum health of the character
	[SerializeField]
	private float baseHealth; //Base health of the character
	[SerializeField]
	private float baseHealthRegen; //base health regen of the character
	[SerializeField]
	private float healthRegen; //current health regen of the character
	[SerializeField]
	private float healthRegenRate;//the rate at which health regens
    [SerializeField]
    private float currentStamina ;//current stamina of the character
	[SerializeField]
	private float maxStamina; //maximum stamina of the character
	[SerializeField]
	private float baseStaminaRegen; //base stamina regen of the character
	[SerializeField]
	private float baseStamina; //base stamina of the character
	[SerializeField]
	private float staminaRegen;//current stamina regen of the character
	[SerializeField]
	private float staminaRegenRate;//rate the stamina regens
	[SerializeField]
	private float baseArmor; //base armor of the character
	[SerializeField]
	private float currentArmor; //current armor of the character.
    //Constant representing the maximum health a character can have
	public const  float MAX_POSSIBLE_HEALTH = 3000f;
    //Constant representing the maximum possible health a character can have.
	public const float MAX_POSSIBLE_STAMINA = 3000f;
    //dfdf
    //Constructor for the character stats class.
	public CharacterStats(float bHealth,float health, float hRegen, float hRegenTime, float bStamina, float stamina,float sRegen, float sRegenTime, float armor){
		this.baseHealth = bHealth;
		this.currentHealth = health;
		this.maxHealth = health;
		this.baseHealthRegen = hRegen;
		this.healthRegen = hRegen;
		this.healthRegenRate = hRegenTime;

		this.baseStaminaRegen = sRegen;
		this.baseStamina = bStamina;
		this.currentStamina = stamina;
		this.maxStamina = stamina;
		this.staminaRegen = sRegen;
		this.staminaRegenRate = sRegenTime;

		this.baseArmor = armor;
		this.currentArmor = armor;


	}
    //Method to reset character stats.
	public void resetStats(){
		this.currentArmor = baseArmor;
		this.currentHealth = baseHealth;
		this.healthRegen = baseHealthRegen;
		this.staminaRegen = baseStaminaRegen;
		this.maxHealth = baseHealth;
		this.currentStamina = baseStamina;
		this.maxStamina = baseStamina;
	}


    //Method to add health to the character stats
	private void addHealth(float health){
        //Check if the health is greater than 0
		if (health > 0f) {
            //add passed in health to the current health
			this.currentHealth += health;
            //if the current health is greater than the maximum health then cap the health at the maximum health.
			if (this.currentHealth > this.maxHealth) {
				this.currentHealth = maxHealth;
			}
		}
	}
    //Remove health from the player stats
	private void removeHealth(float health){
        //check if passed in health is > 0
		if (health > 0f) {
            //subtract passed in health from current health
			this.currentHealth -= health;
            //If the current health is less than zero set health regen to 0 (dead!!!) and set current health to 0
			if(this.currentHealth <=0f){
				this.currentHealth = 0f;
				this.healthRegen= 0f;
			}
		}
	}
    //method to add stamina
	private void addStamina(float stamina){
        //Check if the stamina is greater 0
		if (stamina > 0f) {
            //Add passed in stamina to the current stamina
			this.currentStamina += stamina;
            //If the current stamina is greater than the maximum then cap the current stamina to the maximum
			if (this.currentStamina > this.maxStamina) {
				this.currentStamina = maxStamina;
			}
		}
	}
	//Method to remove stamina
	private void removeStamina(float stamina){
        //if the stamina is greater than 0
		if (stamina > 0f) {
            //subtract the passed in stamina from the current
			this.currentStamina -= stamina;
            //If the stamina is less than 50 then leave it at 50 to prevent it from going to zero.
			if(this.currentStamina <=50f){
				this.currentStamina = 50f;
			}
		}
	}
    //method to regen health based on the health regen stat
	public void regenHealth(){
		this.addHealth (this.healthRegen);
	}
    //method to regen stamina based on the stamina regen stat
	public void regenStamina(){
		this.addStamina (this.staminaRegen);
	}

    //method to spend stamina 
	public void spendStamina(float stamina){
		this.removeStamina (stamina);
	}
    //method to add armor to the armor stat
	public void addArmor(float armor){
		if (armor > 0f) {
			this.currentArmor += armor;
		}
	}
    //methodto remove armor from the armor stat
	public void removeArmor(float armor){
		if (armor > 0f) {
			this.currentArmor -= armor;
			if(this.currentArmor<0f){
				this.currentArmor = 0f;
			}
		}
	}
    //Method to add to the max health stat
	public void addMaxHealth(float value){
        //if the value is greater than 0
		if (value > 0f) {
            //Add to the max health
			this.maxHealth += value;
			if(this.maxHealth > MAX_POSSIBLE_HEALTH){
				this.maxHealth = MAX_POSSIBLE_HEALTH;
			}
		}
	}
    //Method to reduce the maximum health of the character.
	public void reduceMaxHealth(float value){
        //if the value is greater thn 0
		if (value > 0f) {
            //subtract the value from the max health
			this.maxHealth -= value;
            //if the max health is less than the base health then keep it at the base health
			if(this.maxHealth < this.baseHealth){
				this.maxHealth = this.baseHealth;
			}
		}
	}

	//Method to add to the maximum stamina.
	public void addMaxStamina(float value){
        //If the value is greater than 0.
		if (value > 0f) {
            //add the passed in value to the max stamina
			this.maxStamina += value;
            // the max stamina is greater than the max possible stamina then cap it to the max possible stamina.
			if(this.maxStamina > MAX_POSSIBLE_STAMINA){
				this.maxStamina = MAX_POSSIBLE_STAMINA;
			}
		}
	}
	
    // Method to reduce the maximum stamina.
	public void reduceMaxStamina(float value){
        //If the value is greater than 0
		if (value > 0f) {
            //reduce the max value
			this.maxStamina -= value;
            //If the max stamina is less than the base stamina then set the max value to the base stamina
			if(this.maxStamina < this.baseStamina){
				this.maxStamina = this.baseStamina;
			}
		}
	}
    //Method to deal damage to the character
	public void dealDamage(float damage){
        //If the damage dealt is greater than 0
		if (damage > 0f) {
            //calculate the the percentage of damage reduced based on armor.
			float dmgReduction = (0.06f * this.currentArmor)/(1f+(0.06f*this.currentArmor));
            //calculate the effective damage
			float effectiveDmg = damage*dmgReduction;
            //remove the effective damage.
			this.removeHealth(effectiveDmg);
		}
	}

    //method to get the current health
	public float getCurrentHealth(){
		return this.currentHealth;
	}
    //method to get the current stamina
	public float getCurrentStamina(){
		return this.currentStamina;
	}
    //method to get the maximum health
	public float getMaxHealth(){
		return this.maxHealth;
	}
    //method to get the maximum stamina
	public float getMaxStamina(){
		return this.maxStamina;
	}
    //method to get health regen rate
	public float getHealthRegenRate(){
		return this.healthRegenRate;
	}
    //method to et the stamina regen rate.
	public float getStaminaRegenRate(){
		return this.staminaRegenRate;
	}
    //Method to set the stamina regen rate.
    public void setStaminaRegenRate(float f)
    {
        this.staminaRegenRate = f;
    }

}
