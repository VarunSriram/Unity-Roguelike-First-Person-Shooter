﻿using UnityEngine;
using System.Collections;

public class ChargeCannon : Weapon {

    [SerializeField]
    private float nextFire = 0.0F;
    public bool chargeFlag = true;
  
   

    public GameObject gocharge;

    public override void tapFunction()
    {
        if (Time.time > nextFire)
        {
            //every time you fire a bullet, add to the recoil.. of course you can probably set a max recoil etc..
            recoiler.recoiller();
            nextFire = Time.time + fireRate;
            GameObject go = Instantiate(loadedBullet, bulletEmitter.position, bulletEmitter.rotation) as GameObject;
            go.GetComponent<Bullet>().setMovementStrategy(0);

            go.SetActive(true);
        }
    }

    public override void holdFunction()
    {
        if(chargeFlag && this.player.canChargeAttack())
        {
            charge+=0.5f;

            if (charge >= chargeCount)
            {
                gocharge = Instantiate(loadedBullet, bulletEmitter.position, bulletEmitter.rotation) as GameObject;
                gocharge.GetComponent<Bullet>().setMovementStrategy(1);
                gocharge.GetComponent<Rigidbody>().useGravity = false;
                gocharge.GetComponent<Bullet>().setMovementStrategy(1);
                gocharge.transform.parent = bulletEmitter;
                gocharge.SetActive(true);
                chargeFlag = false;

            }
        }
    }

    public override void releaseFunction()
    {
        Bullet pm = loadedBullet.GetComponent<Bullet>();
        float mR = pm.getMaxRecoil();
        if (charge >= chargeCount)
        {
            recoiler.setMaxRecoil(75f);
            recoiler.setRecoilSpeed(40f);
            gocharge.transform.parent = null;
            this.player.chargeAttack();
            gocharge.GetComponent<Bullet>().enableMovement();
            recoiler.recoiller();
            recoiler.recoiling();
            recoiler.setMaxRecoil(mR);
            recoiler.setRecoilSpeed(mR / 2f);
            chargeFlag = true;
        }

        else
        {
            recoiler.setMaxRecoil(mR);
            recoiler.setRecoilSpeed(mR / 2f);
        }
        charge = 0f;
    }



}
