﻿using UnityEngine;
using System.Collections;

public class Recoil: MonoBehaviour {
	[SerializeField]
	private Transform recoilMod;
	[SerializeField]
	private GameObject weapon;
	[SerializeField]
	private float maxRecoil_x = 20f;
	[SerializeField]
	private float recoilSpeed = 10f;
	[SerializeField]
	private float recoil = 0.0f;
	
	
	
	public void recoiling() {
		if (recoil > 0f)
			
		{
			
			Quaternion maxRecoil = Quaternion.Euler(90f + maxRecoil_x, 180f, 0f);
			
			// Dampen towards the target rotation
			
			recoilMod.rotation = Quaternion.Slerp(recoilMod.rotation, maxRecoil, Time.deltaTime * recoilSpeed);
			
			weapon.transform.localEulerAngles = new Vector3(recoilMod.localEulerAngles.x, weapon.transform.localEulerAngles.y, weapon.transform.localEulerAngles.z);
			
			recoil -= Time.deltaTime;
			
		} else
			
		{
			
			recoil = 0f;
			
			Quaternion minRecoil = Quaternion.Euler(90f, 180f, 0f);
			
			// Dampen towards the target rotation
			
			recoilMod.rotation = Quaternion.Slerp(recoilMod.rotation, minRecoil, Time.deltaTime * recoilSpeed / 2);
			
			weapon.transform.localEulerAngles = new Vector3(recoilMod.localEulerAngles.x, weapon.transform.localEulerAngles.y, weapon.transform.localEulerAngles.z);
			
		}
		
	}
	public float getRecoil(){
		return this.recoil;
	}

	public void setRecoil(float r){
		this.recoil = r;
	}

	public void recoiller(){
		this.recoil += 0.1f;
	}
	public void setMaxRecoil(float f) {
		this.maxRecoil_x = f;
	}
	
	public void setRecoilSpeed(float f) {
		this.recoilSpeed = f;
	}
}