﻿#pragma strict

var amount : float = 0.01;
var maxAmount : float = 0.02;
var Smooth : float = 3;
var SmoothRotation = 2;
var tiltAngleX = 10;
var tiltAngleZ  =40; 
private var def : Vector3;
 
function Start ()
{
    def = transform.localPosition;
}
 
function LateUpdate ()
{
        var factorX : float = -Input.GetAxis("Mouse X") * amount;
        var factorY : float = -Input.GetAxis("Mouse Y") * amount;
 
        if (factorX > maxAmount)
            factorX = maxAmount;
 
        if (factorX < -maxAmount)
                factorX = -maxAmount;
 
        if (factorY > maxAmount)
                factorY = maxAmount;
 
        if (factorY < -maxAmount)
                factorY = -maxAmount;
 
 
        var Final : Vector3 = new Vector3(def.x+factorX, def.y+factorY, def.z);
        transform.localPosition = Vector3.Lerp(transform.localPosition, Final, Time.deltaTime * Smooth);
 
 
        var tiltAroundZ = Input.GetAxis("Mouse X") * tiltAngleX;
        var tiltAroundX = Input.GetAxis("Mouse Y") * tiltAngleZ;
        var target = Quaternion.Euler (0,tiltAroundZ, 0);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, target,Time.deltaTime * SmoothRotation);    
}