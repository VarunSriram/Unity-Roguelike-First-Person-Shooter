﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public bool enableMouseControl = true;
    public bool enablePlayerMovement = true;
    public FPSMouseControl mouseControl;
    public FPSMovement playerMovement;
	// Update is called once per frame
    void Start()
    {
        mouseControl = GetComponent<FPSMouseControl>();
        playerMovement = GetComponent<FPSMovement>();
    }

	void Update () {
        if (enableMouseControl)
        {
            mouseControl.controlAction();
        }
	}

    void FixedUpdate()
    {
        if (enablePlayerMovement)
        {
            playerMovement.controlAction();
        }
    }
}
