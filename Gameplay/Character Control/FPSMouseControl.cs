﻿using UnityEngine;
using System.Collections;

public class FPSMouseControl : MonoBehaviour, IPlayerControl{
    [SerializeField]
    private float lookSpeed = 5.0f;
    [SerializeField]
    private float verticalRotation = 0.0f;
    [SerializeField]
    private float horizontalRotation = 0.0f;
    [SerializeField]
    private float verticalRange = 60.0f;
    [SerializeField]
    Camera playerCam;
   

    
    public void controlAction()
    {
        horizontalRotation = Input.GetAxis("Mouse X") * lookSpeed;
        transform.Rotate(0f, horizontalRotation, 0f);

        verticalRotation -= Input.GetAxis("Mouse Y") * lookSpeed;
        verticalRotation = Mathf.Clamp(verticalRotation, -verticalRange, verticalRange);
        playerCam.transform.localRotation = Quaternion.Euler(verticalRotation, 0f, 0f);

    }
}
