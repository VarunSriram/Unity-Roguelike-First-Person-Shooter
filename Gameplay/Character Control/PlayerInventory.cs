﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerInventory : MonoBehaviour {


    void Start()
    {
        GConsole.AddCommand("inv_equipCC", "Equip Charge Cannon as Primary Weapon", equipChargeCannon);
        GConsole.AddCommand("inv_equipRL", "Equip Rail Lance as Primary Weapon", equipRailLance);

    }

    enum weaponsInGame
    {
        ChargeCannon = 0,
        RailLance
    }

    [SerializeField]
    private List<Weapon> gameWeapons;

    [SerializeField]
    private SmartReticle sR;

    [SerializeField]
    private Weapon rightHand;
    [SerializeField]
    private Item leftHand;
    [SerializeField]
    private List<Item> itemInventory;
    [SerializeField]
    private List<GameObject> bulletInventory = new List<GameObject>();
    private int bulIndex;
    private int invIndex = 0;
    int inventoryCapacity = 10;

    public void equipRightHand(Weapon w)
    {
        rightHand = w;
    }

    public Weapon getRightHand()
    {
        return this.rightHand;
    }

    public void addToInventory(Item i)
    {
        if (itemInventory.Count + 1 < inventoryCapacity)
            itemInventory.Add(i);
    }

    public void removeFromInventory(Item i)
    {
        itemInventory.Remove(i);
    }

    public void scrollInventoryLeft()
    {
        if (invIndex == 0)
        {
            leftHand = itemInventory[itemInventory.Count - 1];
            invIndex = itemInventory.Count - 1;
        }
        else
        {
            leftHand = itemInventory[invIndex - 1];
            invIndex--;
        }
    }

    public void scrollInventoryRight()
    {
        if (invIndex == itemInventory.Count - 1)
        {
            leftHand = itemInventory[0];
            invIndex = 0;
        }
        else
        {
            leftHand = itemInventory[invIndex + 1];
            invIndex++;
        }
    }

    public List<GameObject> getBulletInventory() {
        return bulletInventory;
    }

    public bool hasBullets()
    {
        return bulletInventory.Count == 0;
    }

    public void addBullet(GameObject wep)
    {
        wep.tag = (bulletInventory.Count + 1).ToString();
        bulletInventory.Add(wep);

    }

    public void clearWeapons()
    {
        bulletInventory.Clear();
    }

    public void setBulIndex(int x)
    {
        this.bulIndex = x;
    }

    public int getBulIndex()
    {
        return this.bulIndex;
    }

    public Texture2D getBulletSymbol(int x)
    {
        return this.bulletInventory[x].GetComponent<Bullet>().getSymbol();
    }

    public Color getBulletColor(int x)
    {
        return this.bulletInventory[x].GetComponent<Bullet>().getGunColor();
    }

    public void equipChargeCannon()
    {
        foreach(Weapon w in gameWeapons)
        {
            w.gameObject.SetActive(false);
        }

        gameWeapons[(int)weaponsInGame.ChargeCannon].gameObject.SetActive(true);
        this.rightHand = gameWeapons[(int)weaponsInGame.ChargeCannon];
        sR.gun = gameWeapons[(int)weaponsInGame.ChargeCannon].transform;
    }

    public void equipRailLance()
    {
        foreach (Weapon w in gameWeapons)
        {
            w.gameObject.SetActive(false);
        }

        gameWeapons[(int)weaponsInGame.RailLance].gameObject.SetActive(true);
        this.rightHand = gameWeapons[(int)weaponsInGame.RailLance];
        sR.gun = gameWeapons[(int)weaponsInGame.RailLance].transform;
    }

    public string equipChargeCannon(string param)
    {
        this.equipChargeCannon();
        return "Equiped Charge Cannon";
    }

    public string equipRailLance(string param)
    {
        this.equipRailLance();
        return "Equiped Rail Lance";
    }

    
}
