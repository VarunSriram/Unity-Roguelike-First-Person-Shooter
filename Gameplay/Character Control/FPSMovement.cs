﻿using UnityEngine;
using System.Collections;

public class FPSMovement : MonoBehaviour, IPlayerControl
{
    public Player player;
    public bool limitDiagonalSpeed = true;
    public float walkSpeed = 6.0f;
    public float runSpeed = 11.0f;
    public float dashSpeed = 100f;
    public bool allowDodge = true;
    // Units that player can fall before a falling damage function is run. To disable, type "infinity" in the inspector
    public float fallingDamageThreshold = 10.0f;
    public bool toggleRun = false;
    public bool allowSprint = true;
    public float jumpSpeed = 8.0f;
    public bool allowJump = true;
    public bool allowFallDmg = false;
    public float gravity = 20.0f;
    // If the player ends up on a slope which is at least the Slope Limit as set on the character controller, then he will slide down
    public bool slideWhenOverSlopeLimit = false;
    // If checked and the player is on an object tagged "Slide", he will slide down it regardless of the slope limit
    public bool slideOnTaggedObjects = false;
    public float slideSpeed = 12.0f;
    // Small amounts of this results in bumping when walking down slopes, but large amounts results in falling too fast
    public float antiBumpFactor = .75f;
    // Player must be grounded for at least this many physics frames before being able to jump again; set to 0 to allow bunny hopping
    public int antiBunnyHopFactor = 1;
    // If checked, then the player can change direction while in the air
    public bool airControl = false;


    private Vector3 moveDirection = Vector3.zero;
    private bool grounded = false;
    private Transform myTransform;
    private CharacterController controller;
    [SerializeField]
    private float speed;
    private RaycastHit hit;
    private float rayDistance;
    private float slideLimit;
    private Vector3 contactPoint;
    private bool falling;
    private float fallStartLevel;
    private bool playerControl = false;
    private int jumpTimer;
    private float dashTime = 0.225f;

    void Start()
    {
        player = GetComponent<Player>();
        controller = GetComponent<CharacterController>();
        myTransform = transform;
        speed = walkSpeed;
        rayDistance = controller.height * .5f + controller.radius;
        slideLimit = controller.slopeLimit - .1f;
        jumpTimer = antiBunnyHopFactor;
    }

    public void controlAction()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        float inputModifyFactor = (inputX != 0.0f && inputY != 0.0f && limitDiagonalSpeed) ? .7071f : 1.0f;

        if (grounded)
        {
            bool sliding = false;
            // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
            // because that interferes with step climbing amongst other annoyances
            if (Physics.Raycast(myTransform.position, -Vector3.up, out hit, rayDistance))
            {
                if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    sliding = true;
            }
            // However, just raycasting straight down from the center can fail when on steep slopes
            // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
            else
            {
                Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);
                if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                    sliding = true;
            }

            // If we were falling, and we fell a vertical distance greater than the threshold, run a falling damage routine
            if (falling)
            {
                falling = false;
                if (allowFallDmg)
                {
                    if (myTransform.position.y < fallStartLevel - fallingDamageThreshold)
                        FallingDamageAlert(fallStartLevel - myTransform.position.y);
                }
            }

            // If running isn't on a toggle, then use the appropriate speed depending on whether the run button is down

            if (!toggleRun)
            {
                if (allowSprint)
                {
                    if (Input.GetButton("Run"))
                    {
                        player.setStaminaRegenFlag(false);
                        player.onSprint();
                        if (!player.isTired())
                        {
                            speed = runSpeed;
                        }
                        else
                        {
                            float x = player.getCharacterStats().getCurrentStamina();
                            float a = 50f;
                            float b = player.getMinimumRunStamina();
                            float c = 5f;
                            float d = runSpeed;
                            speed = (x - a) / (b - a) * (d - c) + c;
                        }
                    }
                    else
                    {
                        player.setStaminaRegenFlag(true);
                        speed = walkSpeed;
                    }
                    
                }
            }
            // If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
            if ((sliding && slideWhenOverSlopeLimit) || (slideOnTaggedObjects && hit.collider.tag == "Slide"))
            {
                Vector3 hitNormal = hit.normal;
                moveDirection = new Vector3(hitNormal.x, -hitNormal.y, hitNormal.z);
                Vector3.OrthoNormalize(ref hitNormal, ref moveDirection);
                moveDirection *= slideSpeed;
                playerControl = false;
            }
            // Otherwise recalculate moveDirection directly from axes, adding a bit of -y to avoid bumping down inclines
            else
            {
                moveDirection = new Vector3(inputX * inputModifyFactor, -antiBumpFactor, inputY * inputModifyFactor);
                moveDirection = myTransform.TransformDirection(moveDirection) * speed;
                playerControl = true;
            }

            if (!Input.GetButton("Jump"))
                jumpTimer++;
            else if (jumpTimer >= antiBunnyHopFactor)
            {
                if (allowJump)
                {
                    if (player.canJump())
                    {
                        player.onJump();

                        moveDirection.y = jumpSpeed;
                        jumpTimer = 0;
                    }
                }
            }

        }

        else
        {
            // If we stepped over a cliff or something, set the height at which we started falling
            if (!falling)
            {
                falling = true;
                fallStartLevel = myTransform.position.y;
            }

            // If air control is allowed, check movement but don't touch the y component
            if (airControl && playerControl)
            {
                moveDirection.x = inputX * speed * inputModifyFactor;
                moveDirection.z = inputY * speed * inputModifyFactor;
                moveDirection = myTransform.TransformDirection(moveDirection);
            }
        }

        if (Input.GetButtonDown("Dash") && Input.GetAxis("Horizontal") != 0f)
        {
            if (allowDodge)
            {
                player.onDodge();
                StartCoroutine(dasher(this.dashTime, Input.GetAxis("Horizontal")));
            }
        }

        // Apply gravity
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller, and set grounded true or false depending on whether we're standing on something
        grounded = (controller.Move(moveDirection * Time.deltaTime) & CollisionFlags.Below) != 0;
    }

    

    void FallingDamageAlert(float fallDistance)
    {
        print("Ouch! Fell " + fallDistance + " units!");
    }

    void Dash(float input)
    {
        playerControl = false;
        //If input is left
        if (input < 0f)
        {
            //Dash left
            grounded = (controller.Move(dashSpeed * -transform.right * Time.deltaTime) & CollisionFlags.Below) != 0;
            //If input is right.
        }
        else if (input > 0f)
        {
            grounded = (controller.Move(dashSpeed * transform.right * Time.deltaTime) & CollisionFlags.Below) != 0;
        }

        playerControl = true;
    }

    IEnumerator dasher(float delay, float input)
    {
        float startTime = Time.time;
        while (startTime + delay > Time.time)
        {
            this.Dash(input);
            yield
                return null;
        }

    }

}
