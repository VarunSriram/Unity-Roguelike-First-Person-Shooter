﻿using UnityEngine;
using System.Collections;

public class WorldTeleporter : MonoBehaviour {
    public Transform destTransform;
    public HidingTransformTeleporter tp;
    

    public void setTPDestination(Transform t,GameObject host, GameObject destination)
    {
        tp.hostWorld = host;
        tp.destinationWorld = destination;
        tp.dest = t;
    }
}
