﻿using UnityEngine;
using System.Collections;

public class DayNightCycle: MonoBehaviour {
	//Light object that acts as our sun.
	[SerializeField]
	private Light sun;
    
    
	//public Light nightLight;
	//Color for our global fog.
	[SerializeField]
	private Color fogColor;

    public Color defaultFogColor;
	//Number of seconds to represent 24 hours.
	[SerializeField]
	private float secondsInFullDay = 120f;

	//float to represent the current time of day represent as a value betwreen 0 and 1.
	[SerializeField]
	[Range(0, 1)]
	private float currentTimeOfDay = 0;
	[HideInInspector]
	//float to represent a time mulriplier value
	public float timeMultiplier = 1f;
	[HideInInspector]
	//Color to store a color offset when changing the fog color.
	public Color fogMultiplier = Color.white;
	//float to store the daytime sun intensity.s
	float sunInitialIntensity;
    private Color defaultColor;
    public Material starMaterial;
    
	void Start() {
       
        GConsole.AddCommand("dyc_set_time", "Sets the time based on given float between 0 and 1 ", dycTimeCommnd);
		//On startup the initial intensity is set to the suns given intensity.
		sunInitialIntensity = sun.intensity;
        //Dimness is set from the max particles;
       
		//The fog color is set to the one specified in the rendersettings. 
		fogColor = RenderSettings.fogColor;
        defaultFogColor = fogColor;
        //nightLight.enabled = false;
        this.defaultColor = this.sun.color;
     //   pr = stars.GetComponent<ParticleSystemRenderer>();
       // stars.gameObject.SetActive(false);
    }
	
	void Update() {
		//Every frame update the suns position, the intensity of the sun and the fog color.
		UpdateSun();
		//update the time of day
		currentTimeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplier;
		//IF the curren value of time is aabove one reset to zero.
		if (currentTimeOfDay >= 1) {
			currentTimeOfDay = 0;
		}
	}
	
	void UpdateSun() {
       
        //Update the suns rotation around the x axis based on the current time of day as a scalar.
        sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 170, 0);
		
		//adjusts the intensity multiplier depending if the day is reaching dusk or dawn or night time.
		float intensityMultiplier = 1;
		//If it is night time the suns intensity si zero.
		if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f) {
			intensityMultiplier = 0f;
           
            //nightLight.enabled = true;
           
        }
		//If it is dawn time then gradually increase the suns intensity based on current time of day
		else if (currentTimeOfDay <= 0.25f) {
			intensityMultiplier = Mathf.Clamp01((currentTimeOfDay - 0.23f) * (1 / 0.02f));
            //nightLight.enabled = false;
            // stars.gameObject.SetActive(false);
           

        }
		//If it is dusk then gradually decrease the suns intensity until it reaches zero.
		else if (currentTimeOfDay >= 0.73f) {
			intensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
            // stars.gameObject.SetActive(true);
           

           
        }
       
        //Scale the suns intensity based on the intensity multiplier.
        sun.intensity = sunInitialIntensity * intensityMultiplier;
		//sun.color.r = sun.color.r * (1f - intensityMultiplier);
		//Scale the rgb values of the global fog color base on the intensity multiplier.
		fogMultiplier.r = intensityMultiplier;
		fogMultiplier.g = intensityMultiplier;
		fogMultiplier.b = intensityMultiplier;
		//Set the global fog color based on the scaled fog color.
		RenderSettings.fogColor = fogColor * fogMultiplier;

      
        starMaterial.SetColor("_TintColor", new Color(1f, 1f, 1f, 1f - intensityMultiplier));
        

    }
	//returns current time of day.
	public float getTimeOfDay() {
		return this.currentTimeOfDay;
	}
	//sets teh tiem of day.
	public void setTimeOfDay(float time) {
		this.currentTimeOfDay = time;
	}

    public Light getSun()
    {
        return this.sun;
    }

    public Color getDefaultColor()
    {
        return this.defaultColor;
    }

    public Color getFogColor()
    {
        return this.fogColor;
    }

    public void setDefaultFogColor()
    {
        this.fogColor = this.defaultFogColor;
    }

    public void setFogColor(Color c)
    {
        this.fogColor = c;
    }

    string dycTimeCommnd(string param)
    {
        float f = 0f;
        bool canConvert = float.TryParse(param, out f);
        if (canConvert == true)
        {
            f = float.Parse(param);
            this.setTimeOfDay(f);

            return ("Time is set to: " + ((f*24f).ToString()));
        }
        else
        {
            return ("Invalid Input");
        }
    }
}