﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnvironmentTrigger : MonoBehaviour
{
    public BiomeAssetPackage biomeAssets;
    public List<GameObject> remainingDuns;
    public GameObject hostDun;
    public RaymarchedClouds cloudSystem;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            
            RenderSettings.skybox = biomeAssets.getSkyBox();
            cloudSystem.allignClouds(other.transform.position);
}
    }
}
