﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SpeedTreeMaterialManager : MonoBehaviour {

    public List<Material> leafs;
    public Color test;

    public void setLeafColor(Color c)
    {
        leafs= this.getLeafMaterials();
        if (leafs.Count > 0){
            foreach (Material m in leafs)
            {
                m.SetColor("_Color", c);
            }

        }

    }

    public void restoreMaterials()
    {
        this.setLeafColor(Color.white);
    }

    public List<Material> getLeafMaterials()
    {
        List <Material> package = new List <Material>();
        Renderer[] mrs = gameObject.GetComponentsInChildren<Renderer>();
      
        foreach(Renderer mr in mrs)
        {
            Material[] materials = mr.materials;
            foreach(Material m in materials)
            {
                string MName = m.name;
                if (MName.Contains("Leaves") || MName.Contains("Fronds") || MName.Contains("Billboard"))
                {
                    package.Add(m);
                }
            }
           
        }

        return package;
    }

   
    

}
