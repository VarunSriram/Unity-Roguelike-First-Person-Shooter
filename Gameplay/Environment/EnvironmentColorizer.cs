﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnvironmentColorizer : MonoBehaviour {
    [SerializeField]
    private int pickedIndex;
    public Color[] skyColors;
    public Color[] atmosphereColors;
    public Color[] atmosphereShadeColor;
    public Color[] lightingColors;
    public Color[] chlorophyllColors;
    public string seed;

    public Light sun;
    public Color defaultSunColor;
    public Material cloudMat;
    public Color defaultCloudBase;
    public Color defaultCloudShading;
    public DayNightCycle dyc;


    private List<ColorPallet> colorPallets;

	// Use this for initialization
	void Start () {
        pickedIndex = -1;
        colorPallets = new List<ColorPallet>();
        int count = skyColors.Length;
        for(int i = 0; i < count; i++)
        {
            colorPallets.Add(new ColorPallet(skyColors[i], atmosphereColors[i], atmosphereShadeColor[i], lightingColors[i], chlorophyllColors[i])); 
        }
        defaultSunColor = sun.color;
        defaultCloudBase = cloudMat.GetColor("_BaseColor");
        defaultCloudShading = cloudMat.GetColor("_Shading"); 
	}

    public void resetAssets(List<BiomeAssetPackage> usedBiomes)
    {
        foreach (BiomeAssetPackage bap in usedBiomes)
        {
           bap.resetMaterials();
        }
        sun.color = defaultSunColor;
        cloudMat.SetColor("_BaseColor", defaultCloudBase);
        cloudMat.SetColor("_Shading", defaultCloudShading);
        
    }

    public void pickColorPallete(string seed,bool useDefault)
    {
        if (!useDefault)
        {

            System.Random rnd = new System.Random(seed.GetHashCode());
            pickedIndex = rnd.Next(0, skyColors.Length);

        }

        else
        {
            pickedIndex = -1;
        }
        
    }

    public void applyGlobalSkyColors()
    {
        if(pickedIndex < 0)
        {
            sun.color = defaultSunColor;
            cloudMat.SetColor("_BaseColor", defaultCloudBase);
            cloudMat.SetColor("_Shading", defaultCloudShading);
            dyc.setDefaultFogColor();
        }
        else
        {
            sun.color = colorPallets[pickedIndex].lightingColor;
            cloudMat.SetColor("_BaseColor", colorPallets[pickedIndex].atmosphereBaseColor);
            cloudMat.SetColor("_Shading", colorPallets[pickedIndex].atmosphereShadeColor);
            dyc.setFogColor(colorPallets[pickedIndex].skyColor*new Color(0.75f,0.75f,0.75f));
        }
    }

    public ColorPallet getPickedPallet()
    {    
            return colorPallets[pickedIndex];  
    }

    public bool isUsingDefaultColorPallet()
    {
        return pickedIndex < 0;
    }





    
}

public struct ColorPallet
{
    public Color skyColor;
    public Color atmosphereBaseColor;
    public Color atmosphereShadeColor;
    public Color lightingColor;
    public Color chlorophyllColor;

    public ColorPallet(Color sky, Color atmosphere, Color shadeColor, Color lighting, Color chlorophyll)
    {
        skyColor = sky;
        atmosphereBaseColor = atmosphere;
        atmosphereShadeColor = shadeColor;
        lightingColor = lighting;
        chlorophyllColor = chlorophyll;
    }
}
