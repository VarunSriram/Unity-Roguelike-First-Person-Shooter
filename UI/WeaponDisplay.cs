﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WeaponDisplay : MonoBehaviour, IView {
    public int selectedIndex = 0;
    public Color selectedColor;
    public List<Sprite> spriteList;
    public Image weaponDisplay;
	// Use this for initialization
	void Awake () {
        weaponDisplay = GetComponent<Image>();
       // weaponDisplay.sprite = spriteList[0];
	}
	
	// Update is called once per frame
	

    public void updateView()
    {
        weaponDisplay.sprite = spriteList[selectedIndex];
    }
}
