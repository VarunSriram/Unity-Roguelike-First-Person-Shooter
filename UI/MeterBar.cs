﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MeterBar : MonoBehaviour, IView {


    float maxWidth;
    float height;
    public float width;
    public float currentValue;
    public float maxValue;
    public float nominalValue;
    public float criticalValue;


    public Color nominal;
    public Color warning;
    public Color critical;



    RectTransform hBarTransform;
    Image meter;

    [Range(0f, 1f)]
    public float scaleFactor;

	// Use this for initialization
	void Awake () {
        meter = GetComponent<Image>();
        hBarTransform = GetComponent<RectTransform>();
        maxWidth = Screen.width * scaleFactor;
        height = hBarTransform.sizeDelta.y;
        
	}
	
	

    public void updateView()
    {
        

        width = (currentValue * maxWidth) / maxValue;
        hBarTransform.sizeDelta = new Vector2(width, height);

        if(currentValue > nominalValue)
        {
            meter.color = nominal;
        }
        else if(currentValue > criticalValue && currentValue <= nominalValue)
        {
            meter.color = warning;
        }
        else if(currentValue <= criticalValue)
        {
            meter.color = critical;
        }

    }
}
