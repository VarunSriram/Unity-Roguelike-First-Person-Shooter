﻿using UnityEngine;
using System.Collections;

public class LinearScalerRect : MonoBehaviour, IView {
    public RectTransform rectTransform;
    public float minSize = 25f;
    public float maxSize = 150f;
    public float minValue = 90f;
    public float maxValue = 0f;

    public float value = 90f;
	void Awake () {
        rectTransform = GetComponent<RectTransform>();

	}
    public void updateView()
    {
        float scaleValue = (value - minValue) / (maxValue - minValue) * (maxSize - minSize) + minSize;
        rectTransform.sizeDelta = new Vector2(scaleValue, scaleValue);
    }
}
