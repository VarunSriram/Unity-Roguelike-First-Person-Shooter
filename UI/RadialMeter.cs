﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RadialMeter : MonoBehaviour, IView {
    public Image radialMeter;
    public float value;

	// Use this for initialization
	void Awake () {
        radialMeter = GetComponent<Image>();
	}

    public void updateView()
    {
        radialMeter.fillAmount = value;
    }



	
	
}
