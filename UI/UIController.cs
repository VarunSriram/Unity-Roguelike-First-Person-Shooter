﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {
    public MeterBar staminaBarView;
    public MeterBar healthBarView;
    public LinearScalerRect recoilView;
    public WeaponDisplay weaponView;
    public RadialMeter chargeView;

    public CharacterStats playerStatsModel;
    public Player playerModel;
    public PlayerInventory inventoryModel;
    
	


	// Update is called once per frame
	void Update () {
        staminaBarView.maxValue = playerStatsModel.getMaxStamina();
        staminaBarView.currentValue = playerStatsModel.getCurrentStamina();
        staminaBarView.nominalValue = playerModel.getMinimumRunStamina();
        staminaBarView.criticalValue = 50f;

        healthBarView.maxValue = playerStatsModel.getMaxHealth();
        healthBarView.currentValue = playerStatsModel.getCurrentHealth();
        healthBarView.nominalValue = 0.6f * playerStatsModel.getMaxHealth();
        healthBarView.criticalValue = 0.25f * playerStatsModel.getMaxHealth();

        recoilView.value = inventoryModel.getRightHand().transform.localEulerAngles.x;
        chargeView.value = inventoryModel.getRightHand().getChargeRatio();

        if (inventoryModel.hasBullets())
        {
            weaponView.selectedIndex = inventoryModel.getBulIndex();
        }
        else
        {
            weaponView.selectedIndex = 0;
        }



        recoilView.updateView();
        staminaBarView.updateView();
        healthBarView.updateView();
        weaponView.updateView();
        chargeView.updateView();
    }
}
